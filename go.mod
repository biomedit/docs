module gitlab.com/biomedit/docs

go 1.17

require (
	github.com/google/docsy v0.9.2-0.20240423183652-ec13ca8f7f8a // indirect
	github.com/google/docsy/dependencies v0.7.2 // indirect
)
