---
title: "BioMedIT Training"
linkTitle: "BioMedIT Training"
hide_summary: true
menu:
  main:
    weight: 20
type: docs
---

## Information Security Awareness Training

{{< figure
  src="Information Security Policies & Controls.png"
  alt="sett"
  width="100" >}}

In order to make use of the secure research environments within BioMedIT, we
require users to take two security courses on responsible use of health data,
and more specifically on responsible use of BioMedIT. These courses include
built in quiz questions which must be answered to complete the course.

- **Target audience:** Data Managers, research project members
- **Enrollment:** Automatic via [this link](https://adam.unibas.ch/goto.php?target=crs_1614992_rcode4kvQe9KjXs&client_id=adam)
- **Enrolled users** [Go to the course](https://adam.unibas.ch/goto_adam_crs_1614992.html)

If your organization is not affiliated with SWITCH edu-ID (AAI), send us a
request for access via email to [biomedit@sib.swiss](mailto:biomedit@sib.swiss)

## Getting started with BioMedIT

This training series is intended for new users to get started with the BioMedIT
network.

### sett for beginners

{{< figure src="BioMedIT_sett_Logo.png" alt="sett"  width="200" >}}

These modules provide an overview of sett (secure
encryption and transfer tool) and hands-on knowledge for
performing the most frequent operations using sett.

- **Target audience:** Data Managers, Data Providers
- **Documentation:** [sett User Guide]({{< relref "/docs/sett" >}})

[Go to the course](https://edu.sib.swiss/course/view.php?id=582#section-1)

### BioMedIT Training series role based

#### Guide for Data Providers

{{< figure src="Data Prov_1.png" alt="data managers"  width="180" >}}

This guide assists users from data-providing institutions in managing end-to-end
encrypted data transfers, covering everything from requirements to processes and
operations.

- **Target audience:** Data Providers

[Go to the course](https://edu.sib.swiss/course/view.php?id=582#section-2)

#### Guide for Data Managers

{{< figure src="data_managers.png" alt="data providers"  width="170" >}}

These modules give data managers of research projects an overview of how to get
started with BioMedIT, from the onboarding requirements to the daily operation.

- **Target audience:** Data Managers

[Go to the course](https://edu.sib.swiss/course/view.php?id=582#section-4)

#### Guide for Permission Managers

{{< figure src="permission_manager.png" alt="permission manager" width="100" >}}

Covers the what user roles are in BioMedIT and how to manage project users in
the BioMedIT Portal.

- **Target audience:** Permission Managers

[Go to the course](https://edu.sib.swiss/course/view.php?id=582#section-3)
