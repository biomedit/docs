---
title: "What's new in sett 5.2.0? 🚀"
date: 2024-09-26T08:00:00Z
categories:
  - release
tags:
  - sett
---

We are excited to announce the release of sett version 5.2.0, which brings
several significant enhancements designed to improve usability and streamline
the encryption and transfer of data.

For a full list of changes, please refer to the changelogs:

- [sett-gui/CHANGELOG.md](https://gitlab.com/biomedit/sett-rs/-/blob/main/sett-gui/CHANGELOG.md?ref_type=heads#520---2024-09-18)
- [sett-cli/CHANGELOG.md](https://gitlab.com/biomedit/sett-rs/-/blob/main/sett-cli/CHANGELOG.md?ref_type=heads#520---2024-09-17)

To download the new version, please go to [the download page]({{<ref
"../docs/sett/download_sett/">}}), and choose your distribution.

## Simplified encryption and transfer GUI ✨

The "Encrypt" and "Transfer" tabs have been combined into a single tab, making
the interface cleaner and easier to navigate. You can still choose to encrypt
files first, and transfer them later.

## BioMedIT Portal Integration 🔐

Users can now authenticate through the BioMedIT Portal, unlocking additional
features:

- When creating a package, you can now select eligible approved data transfer
  requests from a drop-down menu, rather than manually entering the DTR number.
- For S3/HTTPS transfers, credentials are automatically retrieved—no need to
  switch back to the Portal to fetch them.

## OpenPGP key expiration date 📅

It is now possible to set an expiration date when generating an OpenPGP
certificate both from sett GUI and sett CLI.

## Additional settings ⚙️

The “Settings” tab of sett GUI now displays more information, such as the
location of your public and private key stores.

## CLI for simultaneous decryption and transfer 📬

Users can now decrypt packages from an S3 object store by specifying the
corresponding arguments.

## Performance and other improvements 🏎️

Additionally, this version includes several bug fixes, as well as security and
performance improvements.

## Support ❤️

We thank you for your continued support and feedback. If you have any questions,
please feel free to reach out to us at <biomedit@sib.swiss>.

The BioMedIT Central Team
