---
title: "What's new in sett 5.5.0? 🚀"
date: 2025-01-22T14:00:00Z
categories:
  - release
tags:
  - sett
---

We are happy to announce the release of **sett version 5.5.0**, which brings -
among others - a new interactive terminal-based user interface.

For a full list of changes, please refer to the changelogs:

- [sett-gui/CHANGELOG.md](https://gitlab.com/biomedit/sett-rs/-/blob/main/sett-gui/CHANGELOG.md?ref_type=heads#550---2025-01-22)
- [sett-cli/CHANGELOG.md](https://gitlab.com/biomedit/sett-rs/-/blob/main/sett-cli/CHANGELOG.md?ref_type=heads#550---2025-01-22)

To download the new version, please go to [the download page]({{<ref
"../docs/sett/download_sett/">}}), and choose your distribution.

## Highlights of the New Release

### Terminal User Interface (TUI) ⌨️

We are excited to introduce a brand-new interactive Terminal User Interface
(TUI) for command-line users! This intuitive interface enables you to encrypt,
transfer, and decrypt packages, with or without authenticated mode.

To launch the TUI, simply run:

```sh
sett
```

Please note that the TUI is a work in progress. While most features are
available , some functionalities, such as authenticated decryption and key
management, are currently missing but will be added in future updates.

Please note that, all previously available CLI subcommands (`encrypt`,
`decrypt`, `keys`, etc.) remain fully supported and functional.

### GUI: Removal of the "Paste from Clipboard" button 🗑️

In the [latest Portal release]({{<ref "portal-9-0-0" >}}), the S3 credentials
tab has been removed. Consequently, the "Paste from Clipboard" button has also
been removed from sett.

For transferring packages within the BioMedIT framework, please use the
[authenticated mode]({{<ref "../docs/sett/authenticated_mode/">}}).

## Support and feedback ❤️

If you have any questions or would like to give us feedback, please feel free
to contact us at <biomedit@sib.swiss>.

The BioMedIT Central Team
