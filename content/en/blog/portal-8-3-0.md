---
title: "What's new in Portal 8.3.0? 🚀"
date: 2024-10-29T08:00:00Z
categories:
  - release
tags:
  - portal
---

We are excited to announce the release of BioMedIT Portal 8.3.0, which is packed
with new features and improved usability.

You may find the full list of changes in the: [CHANGELOG - Portal
8.3.0](https://gitlab.com/biomedit/portal/-/blob/main/CHANGELOG.md?ref_type=heads#830---2024-10-28)

## /new and /edit pages for projects 📝

Projects have evolved a lot since the first versions of the BioMedIT Portal.
They now carry so much information, that creating or editing a project from a
dialog window was not enough anymore. We have introduced dedicated pages for
creating and editing projects, so that information is displayed more clearly.
You may find them at:

```url
https://portal.dcc.sib.swiss/projects/new
```

and

```url
https://portal.dcc.sib.swiss/projects/<project_id>/edit
```

## Enable/disable resources and services for a project 🛠️

You can now enable or disable resources and services for a project. This will
also determine whether the dedicated tabs will be rendered, ensuring your users
are not presented with unnecessary information. You can change this setting from
the project edit page:

```url
https://portal.dcc.sib.swiss/projects/<project_id>/edit
```

## MINIMAL project role becomes NO ROLE 🪪

The MINIMAL role for project has been renamed to NO ROLE. This change is to
better reflect the fact that the user is part of the project, but has no role
assigned. In fact, the NO ROLE is now also mutually exclusive with other roles.

## Support ❤️

We thank you for your continued support and feedback. If you have any questions,
please feel free to reach out to us at <biomedit@sib.swiss>.

The BioMedIT Central Team
