---
title: "What's new in Portal 9.0.0? 🚀"
date: 2025-01-22T08:00:00Z
categories:
  - release
tags:
  - portal
---

We are hyped to announce the release of BioMedIT Portal 9.0.0, which is packed
with new features and improved usability.

You may find the full list of changes in the:
[CHANGELOG](https://gitlab.com/biomedit/portal/-/blob/main/CHANGELOG.md?ref_type=heads#900---2025-01-20).

## Bye bye credentials tab! 👋

We have removed the credentials tab from the data transfer view. This was a
temporary solution to allow data engineers to fetch STS credentials for S3-based
data transfers, while we worked on a more secure, user-friendly solution in
`sett`.

With the advent of authentication in `sett`, `sett` is now able to fetch STS
credentials on behalf of the user. Learn more about the topic by reading
documentation about:

- [`sett`'s authenticated mode]({{<ref "authenticated_mode#">}})
- [`sett`'s guide on encrypting data]({{<ref
  "usage#encrypting-and-sending-data">}})

## One data provider, multiple nodes 🏘️

BioMedIT has discontinued SFTP and the snowflake architecture as a supported
data transfer protocol. Now each data provider can directly send data via
HTTPs/S3 to each of the nodes.

Portal supports this new scenario by allowing each data provider to be linked to
any number of nodes. This also means that node personnel will be able to access
information about users of data providers associated to their node.

## OpenAPI documentation for humans 🤖

Portal now offers an OpenAPI documentation for its API that is easier to access
and read. You can find it at the following URL:
<https://portal.dcc.sib.swiss/schema.html>.
