---
title: "What's new in sett 5.3.0? 🚀"
date: 2024-10-18T12:00:00Z
categories:
  - release
tags:
  - sett
---

We are happy to announce the release of sett version 5.3.0, which brings cool
new features.

For a full list of changes, please refer to the changelogs:

- [sett-gui/CHANGELOG.md](https://gitlab.com/biomedit/sett-rs/-/blob/main/sett-gui/CHANGELOG.md?ref_type=heads#530---2024-10-18)
- [sett-cli/CHANGELOG.md](https://gitlab.com/biomedit/sett-rs/-/blob/main/sett-cli/CHANGELOG.md?ref_type=heads#530---2024-10-18)

To download the new version, please go to [the download page]({{<ref
"../docs/sett/download_sett/">}}), and choose your distribution.

## Decryption from S3 in sett GUI 📂

You can now use sett GUI to stream and directly decrypt data from an S3 object
store, without creating any temporary files in the local file system.

## Update expiration date for OpenPGP keys 📅

You can now update the expiration date for OpenPGP keys using sett GUI or sett
CLI. Both tools will also warn you, starting from three months ahead of the
expiration date, when you try to use that key for signing or decryption.

## Easier UserID definition for new OpenPGP keys 🗝️

When generating a new OpenPGP key in sett CLI, you can now specify the name and
email separately, which makes it easier to define the UserID for the key:

```shell
sett keys generate --name "Alice Smith" --email alice.smith@example.com
```

## `.deb` packages for sett CLI 📦

sett CLI is now available as a `.deb` package for Debian-based distributions!

## Support ❤️

We thank you for your continued support and feedback. If you have any questions,
please feel free to reach out to us at <biomedit@sib.swiss>.

The BioMedIT Central Team
