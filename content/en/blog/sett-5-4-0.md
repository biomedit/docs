---
title: "What's new in sett 5.4.0? 🚀"
date: 2024-12-18T12:00:00Z
categories:
  - release
tags:
  - sett
---

We are happy to announce the release of **sett version 5.4.0**, which brings -
among others - authenticated decryption from S3 object stores to BioMedIT
users.

For a full list of changes, please refer to the changelogs:

- [sett-gui/CHANGELOG.md](https://gitlab.com/biomedit/sett-rs/-/blob/main/sett-gui/CHANGELOG.md?ref_type=heads#540---2024-12-18)
- [sett-cli/CHANGELOG.md](https://gitlab.com/biomedit/sett-rs/-/blob/main/sett-cli/CHANGELOG.md?ref_type=heads#540---2024-12-18)

To download the new version, please go to [the download page]({{<ref
"../docs/sett/download_sett/">}}), and choose your distribution.

## Highlights of the new release

### Authenticated decryption from S3 in sett GUI and CLI 🔓

Decryption from an S3 object store is now available in **authenticated mode**!

To decrypt data directly from an S3 object store, BioMedIT users can now simply
login to portal from the `sett` GUI app ("User Profile" tab), and enter the
name of the package to decrypt and its DTR ID number.

CLI users are not left behind, as authenticated decryption is also available
on the command line:

```sh
# Authenticated data decryption from portal.
 sett decrypt s3-portal --dtr DTR_ID DATA_PACKAGE_NAME.zip
```

### Additional package details displayed in the decryption tab 🔍

When loading a package for decryption, whether from the local filesystem or
a remote S3 object store, additional details about the package are now
displayed:

- Data Transfer ID
- Timestamp
- Custom metadata (if any)

This is in addition to the information that was already (and remains)
displayed:

- Location of package
- Sender and recipients

## Support and feedback ❤️

If you have any questions or would like to give us feedback, please feel free
to contact us at <biomedit@sib.swiss>.

The BioMedIT Central Team
