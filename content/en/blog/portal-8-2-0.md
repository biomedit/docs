---
title: "What's new in Portal 8.2.0? 🚀"
date: 2024-10-01T14:00:00Z
categories:
  - release
tags:
  - portal
---

We are pleased to announce the release of BioMedIT Portal 8.2.0, which is packed
with new features requested by the BioMedIT nodes, along with interface updates,
improved usability, and enhanced performance.

You may find the full list of changes in the: [CHANGELOG - Portal
8.2.0](https://gitlab.com/biomedit/portal/-/blob/main/CHANGELOG.md?ref_type=heads#820---2024-09-27)

## New Features ✨

- PGP Key Information: You can now view PGP key details directly in the user
  profile.
- Data Providers: Additional tabs display the data transfers list for the
  specific Data Provider and the Data Provider users.
- Nodes: Node users have been moved to a separate tab.
- Projects: New "Additional Project Information" and "Services" tabs have been
  added, allowing for more relevant details to the project and tracking of which
  users have access to specific project services.

## Improvements 🛠️

- Navigation: Administration menu items have been bundled together.
- Users in projects: Improved and clearer view of user roles within projects.
- Inactive Users: The page displayed for users with "inactive" status who return
  to the portal has been improved to provide them clearer information.
- Email Notifications:
  - No emails are sent for updates to users with minimal role.
  - Redundant emails for when users are added or deleted to projects have been
    removed.
  - Filenames are now included in data package emails.

## Support ❤️

We thank you for your continued support and feedback. If you have any questions,
please feel free to reach out to us at <biomedit@sib.swiss>.

The BioMedIT Central Team
