---
title: "What's new in Portal 9.1.0? 🚀"
date: 2025-02-12T08:00:00Z
categories:
  - release
tags:
  - portal
---

We are ecstatic to announce the release of BioMedIT Portal 9.1.0, which is
packed with new features and improved usability.

You may find the full list of changes in the:
[CHANGELOG](https://gitlab.com/biomedit/portal/-/blob/main/CHANGELOG.md).

## New Project's User Role History tab 📜

We have added a new tab to the project view, which allows privileged project
users to see the history of users and their roles in the project, including who
added/removed the user, and when. This feature is useful for easily auditing and
tracking changes in the project's user roles.

For more details, see the [dedicated section]({{<ref
"projects#user-role-history-tab">}}) in the Portal user guide.

## Refurbished Data Transfer Log 📬

We have refurbished the data transfer log tab, to make it easier to read and
understand. The tab now shows a tabular overview of all data packages
transferred within the current data transfer, as well as a detailed view of each
single data package - available by clicking on the data package in the tabular
view. The log now also includes information about whether the data package was
deleted, and when.

## Support and feedback ❤️

If you have any questions or would like to give us feedback, please feel free
to contact us at <biomedit@sib.swiss>.

The BioMedIT Central Team
