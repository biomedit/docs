---
title: "What's new in sett 5.6.0? 🚀"
date: 2025-02-18T12:00:00Z
categories:
  - release
tags:
  - sett
---

We’re excited to announce the release of **sett version 5.6.0**,
packed with bug fixes and new features to enhance your experience.

For a complete list of changes, check out the changelogs:

- [sett-gui/CHANGELOG.md](https://gitlab.com/biomedit/sett-rs/-/blob/main/sett-gui/CHANGELOG.md#560---2025-02-18)
- [sett-cli/CHANGELOG.md](https://gitlab.com/biomedit/sett-rs/-/blob/main/sett-cli/CHANGELOG.md#560---2025-02-18)

To download the latest version, visit
[the download page]({{<ref "../docs/sett/download_sett/">}})
and select your preferred distribution.

## New Features & Improvements ✨

### Authenticated Decryption from S3 in TUI 🔓

Decryption from the S3 object store is now available in the Terminal User
Interface (TUI)!

BioMedIT users can now seamlessly decrypt data directly from an S3 object
store. Simply log in to the Portal via the **User** tab, then switch to the
**Decrypt** tab. In the S3 package selection window, choose a **DTR ID**
from the list and enter the package name.

**Tip:** To launch the TUI, just run `sett` in your terminal.

### File Size and Count Display in GUI 📊

The **Encrypt** tab now displays the total size and number of selected files.
This enhancement helps ensure the selected data matches expectations before
proceeding.

## Need Help or Have Feedback? ❤️

We’d love to hear from you! If you have any questions or feedback,
reach out to us at <biomedit@sib.swiss>.

The BioMedIT Central Team
