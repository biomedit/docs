---
title: "BioMedIT Docs"
linkTitle: "BioMedIT Docs"
---

{{% blocks/lead color="primary" %}}
BioMedIT Docs contains user guides, tutorials, and FAQs related to BioMedIT services.
{{% /blocks/lead %}}

{{< blocks/section color="white" type="row" >}}

{{% blocks/feature icon="fa-solid fa-book" title="Work Instructions and SOPs" %}}
Are you already a BioMedIT user?

In our [GIT WIKI](https://git.dcc.sib.swiss/dcc/documentation-biomedit/),
Data Providers and Data Managers can discover guidance on BioMedIT processes
and instructions for daily operations.
{{% /blocks/feature %}}

{{% blocks/feature icon="fa-lightbulb" title="User Guides" url="/docs/" %}}
Discover our open repository of user guides for our tools and applications
{{% /blocks/feature %}}

{{% blocks/feature icon="fa-solid fa-person-chalkboard"
    title="BioMedIT Training" url="/training/" %}}
Explore the training materials covering responsible health data use, along with
demos to help you get started with BioMedIT.
{{% /blocks/feature %}}

{{% blocks/feature icon="fa-solid fa-rss" title="Blog" url="/blog/" %}}
Read the latest news and updates from the BioMedIT team.
{{% /blocks/feature %}}

{{< /blocks/section >}}
