---
title: "Quick links for SWITCH edu-ID"
linkTitle: "Quick links for SWITCH edu-ID"
---

{{% pageinfo %}}

### What is a SWITCH edu-ID?

SWITCH edu-ID is your persistent identity and gives you access to all federated
services.

The account is easy to use, controlled by the user and provides secure access to
academic services.

The service is provided by SWITCH for Swiss universities and their partners.

- As a student, you can use your account during and even after your studies.
- As an employee, you can use your account during and even after your employment
  at a university.
- As a private individual, you can use your account for your entire lifetime
  (e.g. to access library services).

(Source:
[https://help.switch.ch/eduid/faqs/#eduid-about](https://help.switch.ch/eduid/faqs/#eduid-about))

{{% /pageinfo %}}

{{% alert title="Important" color="warning" %}}

**Do not create duplicate accounts!**

If you create duplicates, you are in violation of the [Terms of
Use](https://eduid.ch/web/tou/?lang=en) and risk your accounts being deleted or
merged. You also risk losing account data. Account merging may have
consequences:

- You will have to verify the contact information again.
- Account merging data is sent to services and IdM administrators to solve
  potential problems.
- You will no longer be able to access some services. If you accidentally
  created a duplicate, follow the instructions here:

- [Managing duplicate
  accounts](https://help.switch.ch/eduid/faqs/#de-duplication)

{{% /alert %}}

## Quick Links

- [Create account: how to create a new SWITCH edu-ID
  account](https://eduid.ch/web/registration/)

- [View and modify account data: how to check, add, delete or change account
  data.](https://eduid.ch/web/change-account-data)

- [Reset password: how to set a new password if you have lost or forgotten
  it.](https://eduid.ch/web/reset-password/)

- [Change password: how to set another
  password.](https://eduid.ch/web/change-password)

- [FAQs - SWITCH edu-ID - SWITCH Help](https://help.switch.ch/eduid/faqs/)
