---
title: "SPHN-DCC Confluence"
linkTitle: "SPHN-DCC Confluence"
---

{{% pageinfo %}}

**SPHN-DCC Confluence** is a collaboration space and repository hosted in
Confluence Cloud, for the working and implementation groups, projects and
iniatives.

{{% /pageinfo %}}

## How to request access

To access SPHN-DCC Confluence, users must create an
[Atlassian account](https://support.atlassian.com/atlassian-account/docs/what-is-an-atlassian-account).
Here are steps:

Access the link <https://sphn-dcc.atlassian.net>.
The following page will display

Select the option **Create an account**

{{< figure src="access_1.png" alt="login" width="350" >}}

The following page will display.

Enter your email address and click on **Sign up**

{{< figure src="access_2.png" alt="login" width="350" >}}

Confluence will send you an email with a verification link:

{{< figure src="access_3.png" alt="login" width="350" >}}

Check your mailbox and open the email sent by Confluence.
Open the message and click on **Verify your email**

{{< figure src="access_4.png" alt="login" width="800" >}}

After verifying your email, Confluence will redirect you to the following
screen to finish setting up your account:

1. Enter your **full name**
2. Create a **password**
3. Click on **Continue**

{{< figure src="access_5.png" alt="login" width="400" >}}

The **SPHN-DCC** welcome space should display.

Note that to gain access to specific spaces, you must contact the
space owner to assign you permissions.

## Guides for users

- [Manage your account](https://support.atlassian.com/confluence-cloud/docs/manage-your-account/)
- [Confluence Essentials](https://www.atlassian.com/software/confluence/resources/guides/confluence-essentials/navigate#home-and-main-page)

## Guides for space owners

- [Organize and customize your Confluence space](https://www.atlassian.com/software/confluence/resources/guides/get-started/organize-customize)
- [Set up & manage space permissions](https://www.atlassian.com/software/confluence/resources/guides/get-started/manage-permissions#set-up-and-manage-space-permissions)
