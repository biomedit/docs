---
title: "DCC GIT"
linkTitle: "DCC GIT"
weight: 10
---

{{% pageinfo %}}

The BioMedIT Code Repository Service (based on Git), is accessible to registered
BioMedIT users to create, collaborate and share their application codes with
other BioMedIT users.

{{% /pageinfo %}}

## Login to GitLab

GitLab is available via federated login from BioMedIT Portal's Quick Access
link:

{{< figure src="home_gitlab.png" alt="Gitlab" width="1200" >}}

The following page will be displayed.

Click on **Federated Login**

{{< figure src="gitlab_1.png" alt="Gitlab" width="900" >}}

## Resources

- [Get started with GitLab](https://about.gitlab.com/get-started/)
- [GitLab Learn → Watch videos and self-driven
  demos](https://about.gitlab.com/learn/)
- [GitLab docs → Access step-by-step tutorials and
  guides](https://docs.gitlab.com/)
