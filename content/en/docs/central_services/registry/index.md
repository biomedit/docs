---
title: "Harbor"
linkTitle: "Harbor"
weight: 50
---

{{% pageinfo %}}

Harbor is an open source registry that secures artifacts with policies and
role-based access control, ensures images are scanned and free from
vulnerabilities, and signs images as trusted. Harbor, a CNCF Graduated project,
delivers compliance, performance, and interoperability to help you consistently
and securely manage artifacts across cloud native compute platforms like
Kubernetes and Docker.

{{% /pageinfo %}}

## 1. How to request access

1.1 Registry is available via federated login from the BioMedIT portal's Quick
Access links:

![home](home_harbor.png)

1.2 Click on the button: Login via OIDC provider

![home](login_harbor_1.png)

## 2. Resources

- [Working with projects](https://goharbor.io/docs/2.3.0/working-with-projects/)
- [Project
  configuration](https://goharbor.io/docs/2.3.0/working-with-projects/project-configuration/)
- [Working with
  images](https://goharbor.io/docs/2.3.0/working-with-projects/working-with-images/)
- [Using the API
  explorer](https://goharbor.io/docs/2.3.0/working-with-projects/using-api-explorer/)
