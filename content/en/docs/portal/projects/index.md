---
title: "Projects"
linkTitle: "Projects"
weight: 40
---

In the **Projects** menu option:

- **Project users** find a consolidated view of their **projects**,
  the **data transfers**, and the **tools and resources** enabled
  for the project.
- **Project Leaders** and **Permission Managers** can [**manage project
  users.**](#managing-project-users)
- **Data Managers** of a project can [**submit a data transfer
  request.**](#submitting-a-data-transfer-request)

---

## Projects view

From the project's menu option, users can see the list of their projects.

The projects displayed can be filtered by using the **Search** function.

By clicking over a project, users can display the project's details:

- [Project Details tab](#project-details-tab)
- [Data Transfers tab](#data-transfers-tab)
- [Resources tab](#resources-tab)
- [Services tab](#services-tab)
- [Users tab](#users-tab)
- [Additional information tab](#additional-information-tab)
- [User Role History](#user-role-history-tab)

### Project Details tab

The details tab shows the project's general settings:

- **Code**: A unique identifier for the project

- **Hosting Node**: Name and code of the node where the project is hosted

- **Archived**: Yes / No

- **Expiration Date**: Date when the project expires.

- **Legal Approval Group**: Group responsible to confirm compliance with legal
  basis and approve production DTRs.

- **Data Specification Approval Group**: Group responsible to review data
  transfer's Data Specifications and approve production DTRs.

- **Legal Support Contact**: Contact email address to engage support about
  ethical and legal questions.

- **Enable SSH Access**: Indicates if SSH access has been enabled for the project.

- **Enable Resources**: Indicates if resources have been enabled for the
  project. The resources tab will be shown/hidden accordingly.

- **Enable Services**: Indicates if services have been enabled for the project.
  The services tab will be shown/hidden accordingly.

- **Identity Provider**: Source user identity provider specified for the project.
  By default is [SWITCH edu-ID](/docs/switch-eduid/), but a node-specific pre-defined
  identity providers may be selected.

- **IP Ranges**: IP range(s) from where client connections to access B-spaces
  is permitted.

  If the user is connecting from an IP outside of the IP range,
  the message _"Your IP address is not within the valid IP address ranges of the
  project"_ is displayed.

### Data Transfers tab

The data transfers tab displays the list of data transfer requests submitted
by the project.

- From the **Search** box, users can search for specific data transfer
  requests by any content as search criteria.

- **+DATA TRANSFER**: Function to submit a new Data Transfer Request.
  Note that this option is only visible to the project's Data Manager(s).
  The data transfer request list is also accessible from the
  [Data Transfers]({{<ref "./data_transfers/">}}) menu option.

### Users tab

Displays the list of users and their role in the project.

**Authorized flag**:
![biomedit_training_flag](biomedit_training_flag.png)

This badge is displayed next to a project user when they have completed the
mandatory BioMedIT training modules: [Information Security Awareness
Training](/docs/training/#information-security-awareness-training)

**Terms Of Use Accepted**: `2.3`

This version number represents the last [Terms Of Use](/docs/portal/terms_of_use)
the user accepted.

### Additional information tab

Notes, comments and relevant information details. Note that this field is only
editable by admins (Node Managers and BioMedIT Support).

### Resources tab

Displays the access links to the node-specific tools and applications configured
for the project. It offers these fields:

- **Name**
- **Location** - a valid URL that points to the resource
- **Contact** - a valid email address whom to contact
- **Description** - a simple textfield

These resources are defined _per project_ only. Because of this limitation,
we added the services tab (see below) which offer more flexibility.

### Services tab

Similar to the resources tab but more flexible. Displays the node-specific tools
assigned to a given project as well as the tools assigned to individual members
of that project. The aim is that Portal serves as a central point for node-specific
information. The `data` field (see example below) allows the use of markdown
language which will be automatically nicely rendered as rich text in Portal.

To create a new entry, **node admins** simply need to issue a POST request:

`POST /backend/projects/<project_pk>/services/`

```json
{
    "name": "Special Tools",
    "description": "A collection of special tools for a project",
    "data": "# Title\n*   feat 1 explained\n*   feat 2 explained",  # <- Markdown language
    "state": "CREATED" # valid states: INITIAL, PROGRESS, CREATED, LOCKED, ARCHIVED
}
```

Where the `project_pk` is the primary key of a given project (not the project code).
Node admins and -viewers will then see the service(s) nicely rendered in the
services tab.

To view, update or delete a project service entry, use the

```url
/backend/projects/<project_pk>/services/<service_pk>/
```

endpoint and the `GET`, `PUT` or `DELETE` http verbs. `service_pk` is the automatically
assigned primary key of a service.

In addition, services can also be assigned to **project members**, by sending a
POST request to this endpoint:

`POST /backend/projects/<project_pk>/members/<user_pk>/services`

```json
{
    "name": "Special Tools",
    "description": "A collection of special tools for a project",
    "data": "# Title\n*   feat 1 explained\n*   feat 2 explained",
    "state": "CREATED" # valid states: INITIAL, PROGRESS, CREATED, LOCKED, ARCHIVED
}
```

Where the `project_pk` is the primary key of a given project (not the project code)
and `user_pk` the primary key of the user. If the user is not member of the specified
project, you'll receive a 404 error.

Once a project service is assigned to a project member, users then are able to see
the current state of their assigned project services in the services tab.

To view, update or delete a project service entry, use the

```url
/backend/projects/<project_pk>/members/<user_pk>/services/<service_pk>/
```

endpoint and the `GET`, `PUT` or `DELETE` http verbs. `service_pk` is the automatically
assigned primary key of a service.

**Note 1**: these services serve information purposes only, have no implicit
functionality. In particular, they do not trigger any mails being sent to the user.

**Note 2**: the service name must be unique and a service can only be assigned once
to a project or project member. The service name can be freely chosen.

**Note 3**: currently the `action` field has no meaning or implicit functionality.

**Note 4**: there is no GUI planned to edit service information data.

### User Role History tab

This tab displays a full history of users and their roles in the project,
including who added/removed the user, and when.

This tab is only visible to node admins, node viewers, project leaders and
permission managers.

## User roles

User roles are set for each project. Each authorized BioMedIT user can have several
user roles and can be assigned access to several projects.

### Roles and responsibilities

**Project Leaders (PL)**:

- responsible and accountable for the project
- responsible for the data lifecycle management of the project
- acts as contact point for escalations and security incidents within the project
- responsible to discuss the setup and configuration of the project space for the
  project with representatives of the BioMedIT Nodes
- responsible for case-by-case authorization of Data Transfers out of the B-space
- assigned to the project in the User Role of “Permissions Manager” by default and
  with that is responsible to assign authorized BioMedIT users
- responsible to assign the User Role of “Permissions Manager” to authorized BioMedIT
  users
- responsible to ensure that at least one authorized BioMedIT user was assigned to
  the project space in the User Role of “Data Manager”
- can act as “Permissions Manager” and/or “Data Manager” and/or “(default) User”
- responsible to revoke access rights to the project space
- informed about every user change (add/remove) via email notification
- there can be multiple PLs per project

**Permissions Manager** (PM):

- accountable to the PL
- each project must have at least one project member with the User Role
  of “Permissions Manager”
- responsible for assigning the User Roles of “(default) User” and “Data Manager”
  to authorized BioMedIT users and with that granting them access to the project
  space
- responsible to revoke access rights to the project space
- acts as contact for the new project members to support them with registration and
  authorization as users of BioMedIT

**Data Manager (DM)**:

- accountable to the PL
- each project must have at least one project member with the User Role “Data Manager”
- responsible for unpacking and decryption of data packages within the B-space
- responsible to extract data from the B-space for transfer out of the project space
  only following explicit authorization of the PL
- responsible to place the request(s) for Data Transfer(s) from a Data Provider to
  the project

**User**:

- accountable to the PL
- has access to one or several project spaces following authorization
- responsible for data processing within the B-space

**No Role**:

- accountable to the PL
- can access the project's details in the Portal, but has no access to the B-space.
- mutually exclusive with other roles

### Managing project users

{{% alert title="Note" color="warning" %}}

This option is only visible to users with the **Project Leader** and/or
**Permissions Managers** role.

{{% /alert %}}

When clicking on the **Edit project** button the top-right corner, you will be
taken to edit page.

#### Adding users to a project

1. In the Users field, enter the user's email address and click on **+USER**.
   Note that the user(s) must have an account in the portal.
2. You will be asked to confirm the user's details before adding it to the project.
3. Review the details of the user, and click on **ADD** to confirm it.
4. You will return to the list of project users. Assign a role to the user by
   clicking on the corresponding checkbox(es).
5. Click on **SAVE**.

#### Removing users from a project

**To remove users** from the project, the Project Leader or the Permissions
Manager should:

- Click on the \'**x\'** next to the user's name.
- After any change, click on **SAVE**

## Submitting a Data Transfer Request

{{% alert title="Important" color="warning" %}}

- This option is only available for users with the **Data Manager role**
- Prior to submitting a DTR, SPHN projects are required to prepare the project's
  RDF Schema, external terminologies, and de-identification rules to ensure
  compliance with the SPHN Semantic Interoperability Framework. For more
  information, please consult the [process
  documentation](https://sphn.ch/document/dtr-process-description/).

{{% /alert %}}

To submit a Data Transfer Request (DTR), click on **+Data transfer**. The data
transfer creation form will be displayed.

Complete the required fields:

- **Unlimited / One Package**: Select if the DTR will cover one or multiple data
  transfers

- **Data Provider**: Select the Data Provider from the drop-down list

- **TEST / PRODUCTION**: Select TEST if mock data will be sent, or PRODUCTION if
  the data will contain patient real data

- **Additional Data Recipients**: Enter the email(s) of the data recipient(s)
  and click on
  the **+USER** button. <BR>
  Note that recipients can only be users with the [Data Manager role](/docs/portal/projects/#user-roles)
  in the project.

- **Legal Basis**: If transferring real data (purpose PRODUCTION), enter the
  legal agreement (i.e. DTUA, DTPA) document name

- **Data Specification**: If transferring real data (purpose PRODUCTION),
  include the link to the Data Specification <BR>

  Example:
  <https://git.dcc.sib.swiss/admin/projects/project-space/example-project/data-transfer/-/tree/main/data-transfer-1?ref_type=heads>

Click on **SAVE**.

A new DTR will be created in **`INITIAL`** status. Confirmation emails with the
DTR's details are sent to the DTR requester, recipient(s), and approvers.

<table>
<tr><td>

Confirmation email example: <BR>

**Subject: BioMed-IT Portal: [For information] DTR-999 - Data Transfer Request
from 'Universität ABC' to 'SIB Project' project**

Dear Data Manager (Patricia Fernandez),

Your Data Transfer Request (DTR-999) has been successfully submitted.

DTR details:

- Project Name: SIB Project
- Project Lead: Sergio Guarino (<sergio.guarino@sib.swiss>)
- Data Provider and coordinator: Universität ABC, Data Provider Coordinator
  (<john.smith@abc.ch>)
- Requestor: Patricia Fernandez (<patricia.fernandezpinilla@sib.swiss>)
- Frequency of transfer: One time
- Data to be transferred is real patient data: No

You will be informed about the status of the approval process.

If you have any questions or need support, please contact <biomedit@sib.swiss>.

Kind Regards,

BioMedIT Team

</td>
</tr>
</table>

### DTR Approvals

When the DTR is created, the following approval requests are submitted in
parallel:

**1. Node(s) Approval**: As data processors, the involved node(s) must confirm
the presence of a pre-existing legal basis for the data transfer and ensure that
the necessary technical infrastructure is in place to support it.

**2. Legal Compliance Approval**: For SPHN/NDS projects, facilitated by the DCC
ELSI Help Desk group who verifies the existence of a legal basis (e.g., DTUA,
Consortium agreement) for the data transfer. For non-SPHN projects, a dedicated
local Legal Compliance group may be optionally configured.

**3. Data Specification Approval**: For SPHN projects, the DCC will review and
approve the data specification documents referenced in the 'Data Specification'
link. This process ensures the compliance with the SPHN Semantic
Interoperability Framework.

{{% alert title="Note" color="warning" %}}

The **Legal Compliance Approval (2)** and the **Data Specification Approval
(3)** are exclusively required in transfers of **real data**. In any other case
(test data), they are excluded from the approval workflow.

{{% /alert %}}

<table>
<tr><td>

**Email example:**

**Subject: BioMed-IT Portal: [Action needed] DTR-999 - Data Transfer Request
from 'Universitätsspital ABC' to 'SIB' project**

Dear \<BioMedIT Node\> , \<Legal Compliance Group\> , \<Data Specification
Group\> ,

You are kindly requested to review and approve or reject the Data Transfer
Request associated with the SIB project by using the following Data Transfer
Request Approval form:

<https://portal.dcc.sib.swiss/data-transfers/999>

Below are the details of the Data Transfer Request (DTR-ID):

- Project Name: SIB Project
- Project Lead: Alice Smith (<alice.smith@sib.swiss>)
- Data Provider and coordinator: Universitätsspital ABC, Data Provider
  Coordinator (<john.smith@abc.ch>)
- Requestor: Patricia Fernandez (<patricia.fernandez@sib.swiss>)
- Frequency of transfer: One time
- Data to be transferred is real patient data: Yes

If you have any questions or need support, please contact <biomedit@sib.swiss>

Kind Regards,

BioMedIT Team

</td>
</tr>
</table>

**4. Data Provider Coordinator Approval**: This approval request is triggered
only when the previous approvers, (1),(2) and (3) have submitted theirs.

The Data Provider Coordinator is then requested to confirm if the data delivery
has been approved through their internal governance processes.

When all approvers approve the request, the DTR status changes to `AUTHORIZED`,
and data can then be transferred. Notification emails are sent to all parties
involved in the data transfer:

<table>
<tr><td>

**Subject: BioMed-IT Portal: [For information] DTR-999 - Data Transfer Request
from 'SIB Data Provider' to 'SIB Project' project is approved**

Dear All,

Clearance has been granted to transfer data in accordance with DTR 999 for
project SIB Project (sib_project).

The DP Data Engineer from SIB Data Provider is now authorized to encrypt, sign,
and transfer the data package(s) using the sett tool according with the agreed
procedures to the SIS node. Detailed instructions on how to use sett can be
found at <https://biomedit.gitlab.io/docs/sett/>.

The transferred data will be in accordance with the specification outlined here:
<https://git.dcc.sib.swiss/admin/projects/project-space/sib-project/data-transfer/-/tree/main/data-transfer-1?ref_type=heads>.

Upon completion of the data package transfer, please inform the Project's Data
Manager(s), Patricia Fernandez (<patricia.fernandezpinilla@sib.swiss>), so that
they can confirm the reception, integrity, and successful decryption of the data
package in the B-space.

For any further questions, please don't hesitate to contact <biomedit@sib.swiss>.

Kind Regards

BioMedIT Team

Approval log:

- SIS by James Parker (<james.parker@sis.ch>) at 2024-04-17 08:52:53
- Universitätsspital ABC by John Smith (<john.smith@abc.ch>) at 2024-04-18
  09:35:47
- ELSI Help Desk by Daniel Moore (<daniel.moore@sib.swiss>) at 2024-04-17 08:53:07
- DCC (Data Specification) by Jane Muller (<jane.muller@sib.swiss>) at 2024-04-17
  08:59:40

</td>
</tr>

</table>

If any of them rejects the DTR, its status is set to `UNAUTHORIZED`.

## Monitoring the approval status of a Data Transfer Request

The status of a DTR is displayed in the last column of the DTR list.

### Approval Status

- `INITIAL`: The DTR was submitted, but it has not yet been approved.
- `AUTHORIZED`: All approvers have authorized the DTR. The Data Provider can now
  send the data.
- `EXPIRED`: The Data Provider sent the maximum allowed number of data packages,
  and no additional data can be sent under this DTR ID
- `UNAUTHORIZED`: The DTR was previously authorized but is currently
  unauthorized for some reason (i.e., The BioMedIT Node is offline, problems
  with user permissions, issues on the Data Provider\'s end, etc.)|

## Data Transfer details

When clicking on a particular data transfer, a pop-up window shows additional
information

- [Details tab](#details-tab)
- [Logs tab](#logs-tab)
- [Approvals tab](#approvals-tab)

### Details tab

Displays the attribute values of the DTR as submitted by the project's data
manager.

### Logs tab

Displays the individual data packages transfer logs - routing information and
whether the data has arrived to the project workspace.

### Approvals tab

Displays the current approval status of a DTR by all approval groups.
