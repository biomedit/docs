---
title: "Data Transfers"
linkTitle: "Data Transfers"
weight: 50
---

In the **Data Transfers** menu option:

- **Data Managers** and **Data Providers** can search and view the list of
  submitted Data Transfers Requests (DTRs), monitor their
  [**authorization**](#dtr-approval-status) status and [**transfer**](#logs-tab)
  logs
- **Data Provider Coordinators** can [**approve or reject a
  DTR**](#approving-dtrs)

## DTR Approval status

The approval status of a DTR is displayed in the last column of the table:

- `INITIAL`: The DTR was submitted, but it has not yet been approved by the Data
  Provider and/or the BioMedIT node(s).
- `AUTHORIZED`: All approval groups have authorized the DTR.  The Data Provider
  can now send the data.
- `EXPIRED`: The Data Provider sent the maximum allowed number of data packages,
  and no additional data can be sent under this DTR ID
- `UNAUTHORIZED`: The DTR was previously authorized but is currently
  unauthorized for some reason (i.e., The BioMedIT Node is offline, problems
  with user permissions, issues on the Data Provider\'s end, etc.)

Additional details about which group approved when are shown when clicking on a
specific DTR. See [**approval details**](#approvals-tab)

## Data Transfer details

When clicking on a particular DTR, a pop-up window shows additional information

- [Details tab](#details-tab)
- [Approvals tab](#approvals-tab)
- [Logs tab](#logs-tab)

DTR's details are accessible using the following link structure:
<https://portal.dcc.sib.swiss/data-transfers/>**dtr_id**

### Details tab

Displays the attribute values of the DTR, as submitted by the project's data
manager:

- **Project**: Name of the project
- **Transfer ID**: Unique DTR identifier
- **Data Provider**: the Data Provider code
- **Status**: Approval status of the DTR
- **Max Number of packages**: 1 (single transfer) or UNLIMITED (multiple data
  transfers)
- **Transferred Packages**: Count of packages sent so far under this DTR ID
- **Requestor**: Name of the Data Manager that submitted the DTR
- **Data Specification**: link to the Data Specification
- **Purpose**: TEST (for mock data) / PRODUCTION (for patient real data)
- **Legal Basis**: Legal agreement (i.e. DTUA, DTPA) document name

### Approvals tab

Displays the current approval status of a DTR.

### Logs tab

Displays the individual data packages transfer logs - routing information and
whether the data has arrived to the project workspace.

## Approving DTRs

When the DTR is submitted by a project's Data Manager, the groups involved in
the data transfer will receive a notification by email with all the details, for
their information or for their action (request for approval), depending on the
case, as explained below.

<table>
<tr><td>

**Example:** <BR><BR>
**Subject:** BioMed-IT Portal: [For Information] DTR-999 - Data Transfer Request
from 'Universitätsspital ABC' to 'SIB' project <BR>

Dear Universitätsspital ABC,

We have received the following Data Transfer Request (DTR-999) from the
sib_project Project:

- Project Name: SIB Project
- Project Lead: Alice Smith (<alice.smith@sib.swiss>)
- Data Provider and coordinator: Universitätsspital ABC, Data Provider
  Coordinator (<john.smith@abc.ch>)
- Requestor: Patricia Fernandez (<patricia.fernandez@sib.swiss>)
- Frequency of transfer: One time
- Data to be transferred is real patient data: Yes

Please note that Data Provider approval will only be available upon completion
of approvals from the Nodes, DCC (Data Specification), ELSI Help Desk.

If you have any questions or need support, please contact <biomedit@sib.swiss>

Kind Regards,

BioMedIT Team

</td>

</tr>
</table>

### Approvals

When a DTR is submitted by a Data Manager to a research project, the following
approvals must be collected:

**1. Node(s) Approval**: As data processors, the involved node(s) must confirm
the presence of a pre-existing legal basis for the data transfer and ensure that
the necessary technical infrastructure is in place to support it.

**2. Legal Compliance Approval**: For SPHN/NDS projects, facilitated by the DCC
ELSI Help Desk group who verifies the existence of a legal basis (e.g., DTUA,
Consortium agreement) for the data transfer. For non-SPHN projects, a dedicated
local Legal Compliance group may be optionally configured.

**3. DCC Data Specification Approval**: For SPHN projects, the DCC will review
and approve the data specification documents referenced in the 'Data
Specification' link. This process ensures the compliance with the SPHN Semantic
Interoperability Framework.

{{% alert title="Note" color="warning" %}}

- The approval requests above, (1), (2) and (3), are submitted in parallel <BR>
- The **Legal Compliance Approval (2)** and the **Data Specification Approval
  (3)** are exclusively required in transfers of **real data**. In any other
  case (test data), they are excluded from the approval workflow

{{% /alert %}}

Only when the above groups, (1), (2) and (3) confirm their approval, the DP
Coordinator is requested to submit theirs:<BR>

<table>
<tr><td>

**Example:** <BR> <BR>

**To:** DP Coordinator <BR>
**Subject:** BioMed-IT Portal: [Action needed] DTR-999 - Data Transfer Request
from 'Universitätsspital ABC' to 'SIB' project

Dear Universitätsspital ABC , <BR>

You are kindly requested to review and approve or reject the Data Transfer
Request associated with the SIB project by using the following Data Transfer
Request Approval form:

<https://portal.dcc.sib.swiss/data-transfers/999>

Below are the details of the Data Transfer Request (DTR-ID):

- Project Name: SIB Project
- Project Lead: Alice Smith (<alice.smith@sib.swiss>)
- Data Provider and coordinator: Universitätsspital ABC, Data Provider
  Coordinator (<john.smith@abc.ch>)
- Requestor: Patricia Fernandez (<patricia.fernandez@sib.swiss>)
- Frequency of transfer: One time
- Data to be transferred is real patient data: Yes

If you have any questions or need support, please contact <biomedit@sib.swiss>

Kind Regards,

BioMedIT Team

</td>
</tr>
</table>

By clicking on the link in the email above
(<https://portal.dcc.sib.swiss/data-transfers/><DTR_ID>), DP Coordinators will
be redirected to the BioMedIT Portal to approve or rejects the request.

To approve the DTR, click on **APPROVE** and declare, by clicking on the
checkboxes, if:

- All technical measures are in place to send the data
- There is an existing legal basis for the Data Transfer
- This data delivery has been approved through internal governance processes

Then, click on **APPROVE**.

To reject the DTR, click on **REJECT** and provide a reason for the rejecting.
Then, click on **REJECT**.

When all approvers approve the request, the DTR status changes to `AUTHORIZED`,
and data can then be transferred. Notification emails are sent to all parties
involved in the data transfer:

<table>
<tr><td>

**Subject: BioMed-IT Portal: [For information] DTR-999 - Data Transfer Request
from 'SIB Data Provider' to 'SIB Project' project is approved**

Dear All,

Clearance has been granted to transfer data in accordance with DTR 999 for
project SIB Project (sib_project).

The DP Data Engineer from SIB Data Provider is now authorized to encrypt, sign,
and transfer the data package(s) using the sett tool according with the agreed
procedures to the SIS node. Detailed instructions on how to use sett can be
found at <https://biomedit.gitlab.io/docs/sett/>.

The transferred data will be in accordance with the specification outlined here:
<https://git.dcc.sib.swiss/admin/projects/project-space/sib-project/data-transfer/-/tree/main/data-transfer-1?ref_type=heads>.

Upon completion of the data package transfer, please inform the Project's Data
Manager(s), Patricia Fernandez (<patricia.fernandezpinilla@sib.swiss>), so that
they can confirm the reception, integrity, and successful decryption of the data
package in the B-space.

For any further questions, please don't hesitate to contact
<biomedit@sib.swiss>.

Kind Regards

BioMedIT Team

Approval log:

- SIS by James Parker (<james.parker@sis.ch>) at 2024-04-17 08:52:53
- Universitätsspital ABC by John Smith (<john.smith@abc.ch>) at 2024-04-18
  09:35:47
- ELSI Help Desk by Daniel Moore (<daniel.moore@sib.swiss>) at 2024-04-17
  08:53:07
- DCC (Data Specification) by Jane Muller (<jane.muller@sib.swiss>) at
  2024-04-17 08:59:40

</td>
</tr>

</table>

If any of them rejects the DTR, its status is set to `UNAUTHORIZED`.

## Download data transfer list

By clicking on the download icon at the top-right corner, users can download the
list of DTRs in csv format.
