---
title: "Login"
linkTitle: "Login"
weight: 1
---

To either register for the first time or login to the BioMedIT portal:

1. Go to: <https://portal.dcc.sib.swiss/>. The
   [SWITCH edu-ID](/docs/switch-eduid/) authentication button will appear.
2. Click on **> SWITCH edu-ID**.
3. Enter your **email address** and click on **Continue**. You will be asked to
   enter your password.
4. Your **second authentication method** prompt will appear (SMS, OTP). Enter
   your **Code** and click on **Login**.
5. You will be asked to:
   1. Enter a username of your choice.
   2. Choose which of the emails associated to your
      [SWITCH edu-ID](/docs/switch-eduid/) account to use
      for the BioMedIT Portal.
   3. Agree with the Portal being notified when your affiliation changes.
   4. Agree with the [BioMedIT Portal Private
      Policy](https://www.biomedit.ch/home/privacy/portal-privacy.html).
6. The **BioMedIT Portal Home** will be displayed.
