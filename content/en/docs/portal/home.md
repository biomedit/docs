---
title: "Home"
linkTitle: "Home"
weight: 10
---

As soon as you log in with your [SWITCH edu-ID](/docs/switch-eduid/), the home
page displays the menu bar of the left side, the **Quick Access Panel**, with
short-cuts to central services in the middle, and the **BioMedIT Feed** on the
right, where you may find news and announcements from the BioMedIT Team.

The Portal classifies user accounts by role, and the menu options visible for
each user will depend on the role assigned. A Data Provider, a Project Leader,
or a visitor without a specific role will all see different default menu options

## Quick Access links

- [SPHN Schema Forge](https://sphn-semantic-framework.readthedocs.io/en/latest/sphn_framework/schemaforge.html)
- [SPHN DCC Confluence](/docs/confluence/)
- [Terminology Service](https://sphn-semantic-framework.readthedocs.io/en/latest/sphn_framework/terminology_service.html)
- [GitLab](/docs/portal/central-services/gitlab/)
- [Harbor](/docs/portal/central-services/registry/)
