---
title: "Groups"
linkTitle: "Groups"
weight: 30
---

From the **Groups** menu option, Data Providers can [**manage users and
assign them roles**](#how-to-add-a-user-to-a-data-provider-group).

## Data Provider User roles and responsibilities

The Data Provider roles are intended to provide Data Providing institutions with
the ability to manage the users involved in data transfers to BioMedIT and
assign permission levels according to their responsibilities.

### DP Manager

The DP Manager represents the Data Provider institution within the scope of
BioMedIT. A DP Manager is responsible for the institution's internal processes
related to BioMedIT and delegates responsibilities to other users in their
organization by assigning/removing DP roles. In addition, a DP Manager is
responsible for maintaining and keeping the roles assigned up to date, i.e., if
an employee left or changed responsibilities, and informing the BioMedIT Node
and the DCC accordingly. Finally, the DP Manager delegates the internal
operation coordination to the DP Coordinator.

DP Managers are:

- Responsible for the ensuring data provisioning at the Data Provider institution.
- Responsible for escalations and security incidents within the Data Provider.
- Responsible to assign/revoke the DP roles to other DP users.

### DP Coordinator

The DP Coordinator is responsible for internally coordinating the DP's readiness
to send data, ensuring the legal compliance, i.e., DTUA, DTPA, etc. and all
technical measures required from the DP side are in place for projects in which
Data Provider institution participates. The DP Coordinator must send
confirmation of readiness for data transfers to the DCC when required, and act
as the single point of contact for follow-up questions and/or issues in the
process.

DP Coordinators are:

- Responsible for coordinating internal data transfer processes of the Data
  Provider institution
- Responsible for confirmation of legal readiness for data transfers
- Responsible for confirmation of technical readiness for data transfers

### DP Technical Admin

The DP Technical Admin is responsible for all technical tasks required for the
setup and maintenance of a secure connection from the DP organization to the
BioMedIT Node e.g., network components configuration. He is also responsible to
support DP Data Engineer in the onboarding process. There could be more than one
DP Technical Admin.

DP Technical Admins are:

- Responsible for the configuration of the network components from the DP side
  to permit data transfers.
- Responsible for supporting the onboarding of the DP Data Engineer(s).

### DP Data Engineer

The DP Data Engineer is responsible to prepare, encrypt and sign the data
packages, and transfer them from the DP organization to the assigned BioMedIT
Node. There could be more than one DP Data Engineer.

DP Data Engineers are:

- Responsible for preparing and executing the data package transfers between the
  Data Provider and the assigned BioMedIT Node, following the BioMedIT process
  for secure data transfer.

### DP Security Officer

The DP Security Officer is the designated point of contact from the DP
organization to receive incident and security notifications from BioMedIT such
as (but not limited to) scheduled and non-scheduled downtimes in response to
security incidents, emergency changes and systems upgrades.

DP Security Officers are:

- Responsible for serving as a point of contact for incident, maintenance and
  security notifications from BioMedIT, and distributing them internally.

### DP Viewer

A DP Viewer can monitor the status of data transfers from their DP institution.

## How to add a user to a Data Provider group

{{% alert title="Note" color="warning" %}}

- Only users with the role of [**DP Manager**](#dp-manager) can **add or remove
  users** from their organization.
- The **DP Manager role** can only be assigned **by the DCC** (send an email to
  <biomedit@sib.swiss> for updates).
- Users must have have a BioMedIT **portal account**.

{{% /alert %}}

To add a user to a group:

1. Click on the **edit** button of the group.
2. In the **Users** field, type the user’s email address and click on **+USER**.
3. When no more users need to be added to the group, click on **SAVE**.
