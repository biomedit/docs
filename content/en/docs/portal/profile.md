---
title: "Profile"
linkTitle: "Profile"
weight: 20
---

From the profile menu option, users can check and update their account details,
and manage their [**OpenPGP Keys**](#openpgp-keys) and [**SSH Keys**](#ssh-keys).

## My Profile tab

The **My Profile** tab shows the user's account details.

Account details:

- **Name**: User's name

- **Username**: User [SWITCH edu-ID](/docs/switch-eduid/) identifier

- **Local Username**: The username the user chose during their first login to
  Portal.

- **Email**: Contact email currently selected from the list retrieved from the
  user's [SWITCH edu-ID](/docs/switch-eduid/)

- **Affiliations**: List of organizations (e.g., university, research institute)
  linked to users [SWITCH edu-ID](/docs/switch-eduid/) account

- **IP Address**: The IP address from where the user is connecting

- **Flags**: List of flags enabled for the user

- **Groups**: The list of Groups the user is a member of.

Note that personal details are retrieved from the user's [SWITCH edu-ID](/docs/switch-eduid/).
For any changes to their personal data or affiliated organizations, user may
update their SWITCH edu-ID via [this link](https://eduid.ch/web/change-account-data/2/).

For more resources about SWITCH edu-ID, refer to [SWITCH edu-ID /
Quick Links](/docs/switch-eduid/).

## OpenPGP Keys

From the **OpenPGP Keys** tab, users can see the PGP Keys they have registered
in the Portal, along with their status.

Each key has the following information:

- **Fingerprint**: This is a unique identified for each PGP key. You should make
  sure sure that this value is matching with the fingerprint of your key.
- **User ID**: User ID associated with the PGP key. People usually have their
  full name (and sometimes affiliation) in this field.
- **Email**: Email address associated with the PGP key
- **Status**: The approval status of your key. The list of key statuses is as
  follows:

  - **`PENDING`**: a key approval request was submitted, but the key has not
    been approved yet. This is a manual process and can take from a few hours or
    up couple of days.
  - **`APPROVED`**: key is approved for usage within the BioMedIT network. Only
    approved keys can be used to encrypt, sign and decrypt data packages that
    are transiting via the BioMedIT network.
  - **`APPROVAL-REVOKED`**: approval of the key has been revoked by the BioMedIT
    key validation authority.
  - **`KEY-REVOKED`**: key has been revoked by its owner.
  - **`REJECTED`**: key is not trusted by the BioMedIT key validation authority.
  - **`DELETED`**: key has been removed from the keyserver by its owner.
  - **`UNKNOWN KEY`**: key has not been registered on the BioMedIT portal. If it
    is your own key, please register it. If it is the key of someone else,
    please ask them to register their key.

### Registering a new PGP key

{{% alert title="Important" color="warning" %}}

- The key has been **created**, **uploaded** to the Open PGP keyserver, and
  **verified** with the keyserver (i.e. the keyserver will send you an automated
  email to verify that you have control over the email associated with the PGP
  key). The standard way to generate a key pair in BioMedIT is via the Secure
  Encryption and Transfer Tool - sett GUI application, but PGP keys generated
  using other methods are also accepted (e.g. using the GnuPG command line
  tools).
- The PGP key is using one of the **email addresses** associated to your
  [SWITCH edu-ID](/docs/switch-eduid/).
- You **do not already have an APPROVED key registered on the Portal**. Only one
  key per user can be used for data encryption/decryption at any one time. If
  you wish to register a new key and already have an approved key, please
  contact the DCC to request the revocation of your current key (if you still
  intend to use that key outside of BioMedIT) or revoke it yourself on the
  keyserver (if you want to permanently revoke your key for all applications).

{{% /alert %}}

Once the above prerequisites are met, perform the following steps to register
your key and request its approval:

1. Connect to the BioMedIT portal and go to **Profile** and **OpenPGP Keys** tab.

2. Click on **+OPENPGP KEY**.

3. **Add Key** dialogue box will open. Enter your key's **fingerprint**: please
   copy-paste it from your computer to avoid typing errors. Then click on the
   **green** search icon.

4. If your key is present on the Open PGP keyserver, the user ID and email
   associated with your key will be retrieved and displayed in the dialog box.
   **Please double check that the value is correct**.

5. If the displayed user ID and email values are correct, click on **CONFIRM**.

Your key is now registered and an approval request as been automatically sent to
the DCC. Please note that the verification and approval of your key by the DCC
is a **manual process** and may take **a few days**.

Note that your key's approval status will be `PENDING` until approved by the
DCC. Once approved, the status will change to `APPROVED`, at which point the key
can be used to encrypt and decrypt data to be transferred into the BioMedIT
network.

## SSH Keys

From the **SSH Keys** tab, users can add their public SSH key for a
specific project.

### Registering a new SSH key

1. Click on **+SSH KEY**.
2. Select the **project** from the drop-down list
3. Copy **the content** of your public ssh key
4. click on **SAVE**
