---
title: "Terms Of Use"
linkTitle: "Terms Of Use"
weight: 60
---

In the **Terms Of Use** menu option:

- each node has the opportunity to specify its own terms of use (TOU, a.k.a.
  acceptable use policy)
- each TOU has a **version number**
- each TOU has a **version type**, which is either `MINOR` or `MAJOR`
- every user that uses the services of a node must read and accept the terms of use

## Terms Of Use overview

### Node and portal admins

#### Adding new terms of use

The overview table contains a tab for every node. In every tab, a node admin
can add a new Terms Of Use policy by clicking the `+ TERMS OF USE` button.
The node admin then needs to specify the **node** for which this TOU is valid,
a **version number** (we recommend `major.minor`, e.g. `2.3`) and a **version type**
(`MINOR` or `MAJOR`) which indicates the change compared to the previous document.
The actual **text** for the TOU must be written in
[Markdown](https://www.markdownguide.org/basic-syntax/) format,
which then will be rendered on screen. Once entered, the **text cannot be altered**
anymore. Therefore make sure you create and review the text before you publish
it here.

When a new version of the Terms Of Use is created, all the users of this node
get informed by mail, indicating that a new Terms Of Use is available which
they need to accept soon in order to continue using the service.

Currently there is no further mechanism implemented which would block a user,
for example.

#### Read terms of use

By clicking on a single TOU, a new screen is shown where the details of the TOU
are presented. An additional **users** tab shows the list of users who have already
read and accepted this version of TOU.

#### Download TOU list

A convenient way to get a complete overview is to click on the blue download
button on the TOU overview. This provides a CSV listing all nodes, all versions
and all users that accepted it, including the date it has been accepted.

### Project users

Every project is assigned to a specific node, and every node has its own
Terms Of Use. Users of any given project need to read and accept the TOU
once per node.

#### Read and accept terms of use

In the overview table, all users see a `READ AND ACCEPT` button for the Terms
Of Use they have not accepted yet. The button takes them to the read and accept
page, where they see the rendered Terms Of Use page(s) plus a checkbox:

[x] **I have read and understood the node's terms of use**,

followed by a `CONFIRM` button. Once a user confirms, the node admin gets
notified by email. You cannot _reject_ the Terms Of Use once you accepted.
Once you accepted, the latest version you accepted will appear in the Users
tab of the Projects overview table.

By clicking on a single TOU, a new screen is shown where the details of the TOU
are presented.
