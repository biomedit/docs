---
title: "BioMedIT Portal User Guide"
linkTitle: "BioMedIT Portal User Guide"
weight: 20
---

In the following sections we introduce the BioMedIT portal, how it works and how
to get started using it.

Please use the table of contents menu to the left or below to navigate the topics.

{{< figure src="BioMedIT_Graphic.png" alt="BioMedIT" width="800" >}}

## What is the BioMedIT Portal?

The BioMedIT Portal is the one-stop entry to the BioMedIT Network. It provides
users with a single point of access to the tools and services of the BioMedIT
Network in a secure web-based environment.

With the Portal:

- Project Leads have a consolidated view of their projects and resources,
  enabling them to manage the research team's members and access the project
  space.
- Data Providers and Data Managers oversee data transfers and monitor them.
- Researchers gain access to an expanding collection of collaborative and
  research-oriented tools, which encompass in-house, commercial, and open-source
  resources.

Access is secured with a login with your [SWITCH
edu-ID](/docs/switch-eduid) account and two-factor
authentication.

### Table of Contents
