---
title: "Contact Us"
linkTitle: "Contact Us"
weight: 80
---

It is the contact form to submit feature requests or support to the BioMedIT
Help Desk (<biomedit@sib.swiss>).
