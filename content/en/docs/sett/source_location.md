---
title: "Source code"
linkTitle: "Source code"
weight: 130
---

**sett** is licensed under the [GPLv3 (GNU General Public
License)](https://www.gnu.org/licenses/gpl-3.0) and the source code is
available at <https://gitlab.com/biomedit/sett-rs>

**sett** is developed as part of the [BioMedIT](https://www.biomedit.ch/)
project.
