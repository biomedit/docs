---
title: "Generating SSH keys"
linkTitle: "Generating SSH keys"
weight: 60
---

SSH (Secure SHell) keys are pairs of small text files that are used to securely
identify users and give them access to remote servers, e.g. when transferring
data via SFTP.

SSH keys use [public-key
encryption](https://en.wikipedia.org/wiki/Public-key_cryptography) and always
come in pairs: a public and a private (or secret) key.

- **Public key:** the public key of an SSH key pair is meant to be placed on the
  remote machine to which a user wants to connect. Public keys are
  non-sensitive, and are typically shared by users with system administrators,
  who will place them on the machine to which the user is granted access.
- **Private key:** the private key of an SSH key pair is what uniquely
  identifies a user as the legitimate owner of a public key. In other words,
  having the private key that matches a given public key will give access to any
  machine on which a copy of public key is stored. Private SSH keys are
  sensitive information: they **must be kept private at all times** and should
  **never be shared with anyone** - not even your system administrator. Private
  keys can (and should) be protected by a password, so that even if someone else
  has access to them, they remain unusable.

Generating a new pair of SSH keys must be done only once, and, in the context of
_sett_, is only needed if you intend to transfer data. If you are a user who
only decrypts data, you do not need an SSH key.

Also, do not confuse SSH keys - used to identify yourself on a remote server -
[with OpenPGP keys]({{< ref key_management >}}) - used to encrypt and sign data.

To generate a new SSH key pair, type the command below in your terminal (Linux
and Mac) or PowerShell (Windows users - to start it, search for "powershell"
in the Start menu). Note that you must replace `"alice@example.org"` with your
own email. This will generate an SSH key pair using the
[ed25519](https://www.rfc-editor.org/rfc/rfc8032) algorithm, currently the most
secure public-key algorithm:

```shell
ssh-keygen -a 100 -t ed25519 -C "alice@example.org"
```

**Windows users** who do not have the _ssh-keygen_ command installed, please see
[section below]({{< ref "#install-ssh-keygen-windows" >}}).

When executing the _ssh-keygen_ command above, you will be prompted for the
following information:

1. The name and location where to save the newly created keys. Here you should
   simply accept the default values by not entering anything and pressing
   "Enter" on your keyboard. Default locations are `~/.ssh/id_ed25519` on
   Linux and MacOS, and `C:\Users\%username%\.ssh\id_ed25519` on Windows.
2. A password to protect your private SSH key against illegitimate use. Please
   use a password that is long enough (\>=12 characters) and composed only of
   [ASCII characters](https://en.wikipedia.org/wiki/ASCII).

When the command completes, two new files are produced: `id_ed25519.pub` (the
public key) and `id_ed25519` (the private key).

{{% alert title="Keep the SSH private key secret" color="warning" %}}

Remember that the file containing the private key (`id_ed25519`) must be kept
secret. **Never share it with anyone**, not even your system administrator.

{{% /alert %}}

On Linux and MacOS systems, after the public key is generated, its permissions
must be changed with the following command (this step is not needed for Windows
users):

```shell
chmod 600 ~/.ssh/id_ed25519.pub
```

## Windows users: enabling the ssh-keygen command {#install-ssh-keygen-windows}

Not all versions of windows come with the _ssh-keygen_ command pre-installed. If
this command is unavailable on your machine, please install it as follows:

1. Open the windows settings (shortcut: windows key + i).
2. Search for, and select, "Add an optional feature".
3. Click on "Add a feature".
4. Search for, and select, "Open SSH Client".
5. Click "Install".
6. Restart your computer.

## SSH private key with non-ASCII characters password {#non_ascii_char_in_ssh-section}

Even though it is possible to create an SSH key pair using a password containing
non-ASCII characters, it seems like those characters are encoded differently
between different operating systems.

As an SSH key might be moved to a machine with another operating system, or
encoding might change with a new version, it is impossible to guess the correct
encoding in any case. **For this reason, we recommend not to use non-ASCII
characters to protect SSH private keys.**
