---
title: "Docker image"
linkTitle: "Docker image"
weight: 90
---

In addition to the desktop app and a standalone CLI tool, _sett_ is also
distributed as a Docker image, which can be used to run the tool in a
containerized environment. This page provides instructions on how to use the
_sett_ Docker image.

All images are available in the [GitLab container registry](https://gitlab.com/biomedit/sett-rs/container_registry/6482752).

Pull an image with the following command:

```bash
# You can replace {{< param "sett_version_cli" >}} with a specific version you need.
docker pull registry.gitlab.com/biomedit/sett-rs/sett:{{< param "sett_version_cli" >}}
# Tag the image for easier referencing in later commands.
docker tag registry.gitlab.com/biomedit/sett-rs/sett:{{< param "sett_version_cli" >}} sett:{{< param "sett_version_cli" >}}
```

To start the interactive mode (TUI), run:

```shell
docker run -it --rm sett:{{< param "sett_version_cli" >}}
```

Use `--help` for information on available non-interactive subcommands:

```shell
docker run -it --rm sett:{{< param "sett_version_cli" >}} --help
```

Most _sett_ commands require access to the public and private OpenPGP keys.
Keys can be provided in two ways:

- As individual key files (see the `--signer-path` and `--recipient-path` flags
  in the `encrypt` and `decrypt` subcommands).
- As key stores.

When using individual key files, mount the directory containing these
files as a volume in the container (or mount individual files):

```bash
docker run -it --rm -v ./openpgp:/openpgp:ro -v ./data:/data \
sett:{{< param "sett_version_cli" >}} encrypt local -S /openpgp/alice.pgp -R /openpgp/bob.pgp -o /data /data/README.md
```

Key stores can be mounted in a similar way. However, they must be mounted at the
specific location within the container.

```bash
docker run --rm \
-v ./pub_store:/root/.local/share/pgp.cert.d \
-v ./priv_store:/root/.local/share/sequoia/keystore \
sett:{{< param "sett_version_cli" >}} keys list
```

If you want to use custom locations for the key stores, you can specify them
using environment variables. For more information on key store location see
[CLI settings]({{< ref "settings#cli" >}}).

```bash
docker run --rm \
-e PGP_CERT_D=/certificate_store \
-e SETT_KEYSTORE=/keystore \
-v ./pub_store:/pub_store \
-v ./priv_store:/priv_store \
sett:{{< param "sett_version_cli" >}} keys list
```

Encryption and decryption subcommand might need a password for unlocking the
secret OpenPGP key. This can be provided as an environment variable
(see [CLI settings]({{< ref "settings#cli" >}})):

```bash
# Password as an environment variable
docker run -it --rm -v ./openpgp:/openpgp:ro -v .:/data \
-e SETT_OPENPGP_KEY_PWD="secret" \
sett:{{< param "sett_version_cli" >}} encrypt local -S /openpgp/alice.pgp -R /openpgp/bob.pgp -o /data /data/README.md

# Password from a file
docker run -it --rm -v ./openpgp:/openpgp:ro -v .:/data \
-e SETT_OPENPGP_KEY_PWD_FILE=/pgp_key_pwd \
-v ./password_file:/pgp_key_pwd:ro \
sett:{{< param "sett_version_cli" >}} encrypt local -S /openpgp/alice.pgp -R /openpgp/bob.pgp -o /data /data/README.md
```
