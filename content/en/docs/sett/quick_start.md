---
title: "Quick start guide"
linkTitle: "Quick start"
weight: 10
---

For the complete guide on how to use _sett_, please refer to
[Encrypting, transferring, and decrypting data]({{< ref usage >}})
and [OpenPGP key management]({{< ref key_management >}}).

## GUI (Graphical User Interface)

### Initial setup

1. Download _sett-gui_ from [the download page]({{< ref "download_sett" >}}).
   If you downloaded an installer, install _sett-gui_ by double-clicking on the
   installer file.
1. Run _sett-gui_ by double-clicking on the executable file or by launching the
   installed app.

### Key management

1. If you do not already have a private/public OpenPGP key pair, go to the **Keys**
   tab and create one clicking on **Add** > **Generate new key pair**. See also
   the instructions given in the [Generate a new public/private OpenPGP key pair
   section]({{< ref "key_management/#section-generate-key" >}}).

   You should then see your new key listed in the **Keys** tab, along with
   "Private" label that indicates that the private material for this key is
   present in the local keystore.

   {{% alert title="BioMedIT" color="warning"%}}

   Your OpenPGP key must be registered with the [BioMedIT
   portal](https://portal.dcc.sib.swiss) before it can be used to sign or
   decrypt data packages. Please see the [Register your public OpenPGP key
   in the BioMedIT portal]({{< ref "key_management/#section-register-key" >}})
   section for details.

   {{% /alert %}}

1. If not already done, download the public OpenPGP key of the recipient(s) to whom
   you intend to send data (or from whom you will receive data).
   Go to the **Keys** tab and click on **Add** > **Import from keyserver**. See
   also the instructions given in the [download public OpenPGP keys from the
   keyserver section]({{< ref "key_management/#download-from-keyserver" >}}).
1. Just after downloading the recipient's OpenPGP key, verify it to make sure that
   it is genuine. This can be done by either:
   - If you are a BioMedIT user: verify that the recipient's key
     is labelled with a green **Approved** label. You can also expand the
     details of the key by clicking on the key in the list or on the small
     down arrow button to the right and verify that the **Approval status**
     is set to "Key is approved on Portal", and the **Revocation status** is
     set to "Valid".
   - Alternatively, contact the key owner and verify the key **fingerprint**
     with them.

### Encrypting and sending data

{{% alert title="Authenticated mode (BioMedIT)" color="info" %}}

_sett_ provides "authenticated mode" that simplifies the encryption and transfer
process by providing a list of available Data Transfer Requests (DTRs) and
automatically fetching the required destination parameters and credentials
(only available for the S3 destination).

To access this mode, go to the **Profile** tab and click "Sign in".
You will be redirected to the BioMedIT authentication system.
After successful authentication, proceed to the **Encrypt and Transfer Data** tab.

{{% /alert %}}

1. Go to the **Encrypt and Transfer Data** tab.
1. Add one or more files and directories to encrypt by clicking the **Add
   files** or **Add directories** buttons.
1. **Select sender**: select your own OpenPGP key. This is the key that will be used
   to sign the encrypted data.
1. **Select recipients**: add one or more recipients by selecting them in the
   drop-down. These are the keys that will be used to encrypt the data, i.e.
   only these recipients will be able to decrypt the data.

   {{% alert title="BioMedIT" color="warning" %}}

   The selected recipients must be approved **Data Managers** of the project for
   which data is being encrypted.

   {{% /alert %}}

1. **Data Transfer ID**: specifying a valid **Data Transfer Request ID** is mandatory
   when a data package is transferred into the BioMedIT network. For other
   destinations, the **Data Transfer ID** field can be left empty (or set to any
   arbitrary value), and the **Verify package** checkbox must be disabled (in
   the **Settings** tab).

   {{% alert title="BioMedIT" color="warning" %}}

   The **Verify package** checkbox should always be enabled, since a valid
   **Data Transfer ID** is required by the BioMedIT network.

   {{% /alert %}}

1. **Select destination**: select local and choose a destination directory
   to encrypt to your local file system. Select **s3** or **sftp** to encrypt and
   transfer directly to an S3 object store or an SFTP server, respectively.
1. Click **Encrypt data** (local) or **Encrypt and transfer data** (**s3** or **sftp**)
   to run the encryption workflow on your data.

### Sending existing data packages

1. Go to the **Encrypt and Transfer Data** tab.
1. Select a file to transfer using the add **sett Package** button.
1. Select the **Destination** to be used (**sftp**, **s3**).
1. Enter the required destination parameters.

   {{% alert title="BioMedIT" color="info" %}}

   For transfers into the BioMedIT network, the destination parameters are
   automatically provided by the BioMedIT Portal.

   {{% /alert %}}

1. Click **Transfer data** to start transferring your data package.

### Decrypting data

1. Go to the **Decrypt** tab.
1. Select a data package to decrypt using the **Select Package** button.
1. Specify your desired destination directory.
1. Click on **Decrypt package**.

## CLI (Command Line Interface) and TUI (Terminal User Interface)

Users can choose between **interactive (TUI) mode** and **classical (CLI) mode**.

- **TUI mode** provides a navigable interface for performing encryption,
  transfer, and decryption interactively.
- **CLI mode** allows users to execute specific commands directly.

Run the following command to launch the interactive interface (TUI):

```shell
sett
```

In CLI mode, users can run subcommands to perform specific tasks.
Below are some basic examples.

{{% alert title="sett help" color="info" %}}

Each subcommand includes a help message with available options.
Use `-h` for a short description or `--help` for a detailed explanation.

```shell
sett --help
sett encrypt local -h
sett encrypt local --help
```

{{% /alert %}}

### OpenPGP key management

```shell
# Generate a new key pair
sett keys generate

# Import sender/recipient(s) public keys:
sett keys import from-keyserver alice@example.com
```

### Encrypt and transfer data

```shell
# Data encryption only, saving to local disk
sett encrypt local --signer alice@email.com --recipient bob@example.com --output . FILES_OR_DIRECTORIES_TO_ENCRYPT

# Data encryption and transfer to object store
sett encrypt s3 --signer SIGNER_KEY --recipient RECIPIENT_KEY --recipient-path RECIPIENT_PATH
--endpoint ENDPOINT --bucket BUCKET --access-key ACCESS_KEY --secret-key SECRET_KEY \
FILES_OR_DIRECTORIES_TO_ENCRYPT

# Data encryption and transfer to object store, using portal authentication
sett encrypt s3-portal --signer SIGNER_KEY --recipient RECIPIENT_KEY --dtr DATA_TRANSFER_ID  \
FILES_OR_DIRECTORIES_TO_ENCRYPT

```

### Transfer a sett package

```shell
# Data transfer of an existing sett package

# to S3 object store
sett transfer s3 --endpoint ENDPOINT \
--bucket BUCKET --access-key ACCESS_KEY --secret-key SECRET_KEY \
SETT_PACKAGE_TO_TRANSFER

# to S3 object store, using portal authentication.
# metadata.json inside to package needs to contain a valid data transfer ID
sett transfer s3-portal SETT_PACKAGE_TO_TRANSFER

# to SFTP server
sett transfer sftp --host HOST --username USERNAME --base-path DESTINATION_DIRECTORY --key-path SSH_KEY_LOCATION --key-pwd SSH_KEY_PASSWORD SETT_PACKAGE_TO_TRANSFER
```

### Decrypt a sett package

```shell
# decrypt a local sett package
sett decrypt local SETT_PACKAGE_TO_DECRYPT

# fetch and decrypt package from S3 object store,
# using portal authentication
sett decrypt s3-portal --dtr DTR SETT_PACKAGE_TO_DECRYPT

# fetch and decrypt package from S3 object store
sett decrypt s3 \
--bucket BUCKET --access-key ACCESS_KEY --secret-key SECRET_KEY \
SETT_PACKAGE_TO_DECRYPT
```
