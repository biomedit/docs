---
title: Benchmarks
linkTitle: Benchmarks
weight: 1000
---

## _sett_ performance benchmarks

### Test setup

The benchmarks were run on an **Apple Silicon M3** machine with _16-Core CPUs_,
_128GB memory_, and _2TB disk_. Version **5.2.0** of _sett_ CLI was used.

One single binary file was used, with default compression
([Zstandard](https://facebook.github.io/zstd/), _level 3_). Decreasing the
compression level and/or using text data will influence the results.

Transfer speed is highly dependent on the infrastructure connecting the data
sender and recipients. In these benchmarks, data was transferred to the same
machine sending the data, therefore representing a _best-case_ scenario in
terms of transfer speed.

### Results

The table and figure below present throughput speeds for different _sett_
workflows, all values are averages of 5 runs performed with
[hyperfine](https://github.com/sharkdp/hyperfine):

- **Encrypt:** compress, encrypt package and transfer (S3, SFTP) data.
- **Transfer:** transfer already packaged data. There is no compression and
  encryption involved in this workflow.
- **Decrypt:** decrypt and decompress a data package, either locally or
  streamed from S3.

| Workflow      | Throughput (MB/second) | Throughput (GB/minute) |
| ------------- | ---------------------- | ---------------------- |
| Encrypt local | 230.65                 | 13.84                  |
| Encrypt S3    | 179.98                 | 10.8                   |
| Encrypt SFTP  | 119.79                 | 7.19                   |
| Transfer S3   | 478.68                 | 28.72                  |
| Transfer SFTP | 123.49                 | 7.41                   |
| Decrypt local | 244.74                 | 14.68                  |
| Decrypt s3    | 205.22                 | 12.31                  |

**Throughput as function of file size:** as can be seen, the curves in the
figure below are linear, indicating that the throughput is constant regardless
of the file size.

{{< figure src="benchmarks.jpg" link="benchmarks.jpg"
    caption="(click on the figure to enlarge)" >}}
