---
title: "sett - Secure Encryption and Transfer Tool"
linkTitle: "sett User Guide"
weight: 10
---

[![License](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Source code on GitLab](https://img.shields.io/badge/source_code-GitLab-orange?style=flat-square&logo=Git&logoColor=white)](https://gitlab.com/biomedit/sett-rs)

Welcome to the official **sett** documentation page.

Please use the table of contents menu to the left or below to navigate the
topics.

## Table of Contents
