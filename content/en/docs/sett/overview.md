---
title: "What is sett?"
linkTitle: "What is sett?"
weight: 5
---

**sett** stands for "Secure Encryption and Transfer Tool" and is an
application that facilitates and automates data packaging, encryption, and transfer.
It is written in Rust, a fast and memory-safe programming language.
**sett** is available both as a desktop and a command line application.

**sett** is developed as part of the [BioMedIT](https://www.biomedit.ch/)
project. It is licensed under the
[GPLv3 (GNU General Public License)](https://www.gnu.org/licenses/gpl-3.0)
and the source code is available from its public
[GitLab repository](https://gitlab.com/biomedit/sett-rs).

{{% alert title="BioMedIT" color="info" %}}

When using **sett** to transfer data into the **SPHN BioMedIT network**, a
number of additional constraints apply. These specific instructions are
highlighted throughout this documentation in **BioMedIT labeled boxes**, just
**like this one**.

{{% /alert %}}
