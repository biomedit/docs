---
title: "Frequently asked questions"
linkTitle: "Frequently asked questions"
weight: 120
---

{{< details summary="How do I report a bug?" >}}
To report a problem with sett or if you have a question _not_ covered in
the present documentation please open an issue on our public GitLab
repository <https://gitlab.com/biomedit/sett-rs/-/issues>
{{< /details >}}

{{< details summary="How do I know I am using a proxy?" >}}
On Windows, you can check if you are using a proxy server to access the Internet
by going to **Start > Settings > Network & Internet > Proxy** (Windows 10).
If the slider under **Use a proxy server** is "off", no proxy is being used.

If you are told that you need to set a proxy, input the **Address and Port**
details and click **Save**. When in doubt, please consult your IT department.

On Mac OS the proxy information is located under the **System Preferences >
Network > Advanced > Proxies tab** of the network interface, usually Ethernet
or Wi-Fi.
{{< /details >}}

{{< details summary="How do I run sett behind a proxy?" >}}
In order to run _sett_ behind a proxy, the shell environment variable
`ALL_PROXY` or `HTTPS_PROXY` must be set. This is the recommended and global way
to specify a proxy. Note that, while certain programs support a _proxy_ option
(e.g. `pip` with `--proxy`), there is currently **no such option in sett.**

Example:

```shell
ALL_PROXY=https://host.domain:port sett-gui
```

{{< /details >}}

{{< details summary="Does sett generate logs?" >}}

Yes, it does. _sett_ logs are stored in the following locations:

- Linux: `${XDG_DATA_HOME}/ch.biomedit.sett/log` or `$HOME/.local/share/ch.biomedit.sett/log`
- macOS: `$HOME/Library/Application Support/ch.biomedit.sett/log`
- Windows: `{FOLDERID_RoamingAppData}\ch.biomedit.sett\log`

Log files are rotated daily and named according to the following format:
`<date>.<app>.jsonl`. For example, `2024-08-29.sett-gui.jsonl` and
`2024-08-29.sett.jsonl` for GUI and CLI applications respectively.

Each line within the log file is a `JSON` object corresponding to a single
logging event.
{{< /details >}}

{{< details summary="TUI: Why can't I see all user interface components?" >}}

The interactive TUI mode of _sett_ requires a terminal window with a **minimum
size of 68 x 32** (columns x rows). For the best experience, we recommend using
a terminal window that is **at least 120 x 48**.

If your terminal window is too small, some user interface components may not be
visible. To resolve this, increase the size of your terminal window until the
interface displays correctly.
{{< /details >}}

{{< details summary="TUI: How do I fix an unresponsive password prompt?" >}}

This issue occurs when unlocking an OpenPGP key stored in the GnuPG keyring
while using [pinentry-curses](https://manpages.debian.org/testing/pinentry-curses/pinentry-curses.1.en.html),
a terminal-based password input program (`pinentry`).

To fix this, try one of the following solutions:

- Switch to a graphical `pinentry` program, such as `pinentry-gtk`,
  `pinentry-gnome3`, `pinentry-qt5`, or `pinentry-mac`.
- Migrate your key to the **Sequoia key store** (the default keyring used
  by _sett_) and remove it from the GnuPG keyring.

{{< /details >}}
