---
title: "Authenticated Mode"
linkTitle: "Authenticated Mode"
weight: 70
---

### Why authenticated mode?

In order to simplify the data transfer process, sett offers an authenticated mode.
Once the user is authenticated, the following functionalities are offered:

- list of available DTR (data transfer request) for the logged-in user
- package verification is always enforced
- data transfer from and to a BioMedIT S3-type object storage
  without entering credentials

### Device authorization flow explained

The authenticated mode in sett implements a so-called
**[device authorization flow](https://auth0.com/docs/get-started/authentication-and-authorization-flow/device-authorization-flow)**,
an authorization mechanism usually used by electronic devices with internet connection
that lack any input capacities. In both the CLI and GUI of sett, the user is asked
to visit a specific website (an OIDC provider) to identify himself. While the
user is logging in, the device (in our case: sett) is periodically polling the
website, until the user has successfully logged in. sett then receives a JWT token
(JSON Web Token) from the OIDC provider, which allows it to fetch additional data
from Portal, e.g. the list of available data transfers for the logged-in user, as
well as the credentials used for transferring data to and from the S3 object storage.

The diagram below illustrates how this device authorization flow works in
sett when requesting the list of available DTRs or when up/downloading
data.

{{< figure src="authorization_flow.png" alt="BioMedIT"
caption="Device authorization flow as implemented by sett" >}}

### Glossary

- **CLI**: Command Line Interface. This is how sett is used in a terminal.
- **GUI**: Graphical User Interface. E.g. the standalone sett application that
  runs in its own window.
- **S3 object storage**: the database that (temporarily) stores all sett packages.
  Every node runs its own storage. S3 stands for Simple Storage Service and was
  originally developed by Amazon.
- **SFTP**: Secure File Transfer Protocol. The legacy way to transfer data.
- **OIDC provider**: an system that redirects a user to Switch edu-ID to authenticate
  himself. It can issue tokens which allow sett to fetch information on behalf of
  the user. It plays a crucial role in implementing the device authorization flow.
  OIDC stands for Open ID Connect.
- **JWT token**: a piece of structured data which contains information about an
  authenticated user. It is signed, so a third party (in our case: Portal) can verify
  the integrity of its content. JWT stands for JSON Web Token.
