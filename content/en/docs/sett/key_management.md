---
title: "OpenPGP key management"
linkTitle: "OpenPGP key management"
weight: 30
---

To encrypt and decrypt data, _sett_ uses **public key cryptography**. If you are
not familiar with public key cryptography concepts such as **public and
private** keys, **revocation signatures**, or **keyservers** you are advised to
read [this introductory section]({{< ref "#intro_to_pgp" >}}).

All key management operations can be performed using both the graphical user
interface (GUI) and the command line interface (CLI) of _sett_.

{{% alert title="sett help" color="info" %}}

In the CLI each command and subcommand provides a help message that can be
used to get more information about the available options
(`-h` and `--help` for short and long help message respectively).
For example:

```shell
sett --help
sett encrypt local -h
sett encrypt local --help
```

{{% /alert %}}

## Generate a new public/private key pair {#section-generate-key}

A prerequisite to encrypt, decrypt, and transfer files with _sett_ is to have a
public/private PGP key pair.

{{% alert title="Important" color="warning" %}}

Always keep your **private key**, its **password**, and its **revocation
signature** in a secure location. Never share any of this information with
anyone.

{{% /alert %}}

### Generate a new key pair (GUI)

To generate a new public/private key pair:

1. Go to the **Keys** tab and click on **+ Add**. From the drop-down menu, choose
   **Generate new key pair**. A dialog box will appear.

1. Fill-in the required fields:

   - **Name**: key owner first and last name.
   - **Comment**: an optional comment to help identify the owner of the key
     (e.g., owner's institution).
   - **Email**: enter the email to be associated with the key pair.
   - **Password**: the password must be at least 10 characters long, and it is
     highly recommended that it contain a mix of letters, numbers and special
     characters.
   - Expiration date: a date in the future to ensure that the key is not used
     indefinitely (default: 3 years). It is possible to extend the expiration
     date later.

   {{% alert title="Important" color="warning" %}}

   It is **not possible to retrieve or reset a password from a generated key**.
   Make sure to remember it and store it in a safe place (e.g. a password manager).

   {{% /alert %}}

1. Click **Next** to see the summary.

1. Click "Generate key" to create the new key pair (this can take a few seconds).

1. A new pop-up window will appear to confirm that the key was generated
   successfully and to display the revocation signature associated with the new
   key.

   Copy the revocation signature to a safe location, ideally a password manager,
   and click **Next**.

1. The next screen of the pop-up window allows to upload the public part of the
   key to the keyserver. Make sure to check the checkbox to associate the key
   with your email address. To establish this association, the keyserver will
   send an email to verify that you are the legitimate owner of the email associated
   with the key.
   It also means that other people will now be able to search for your key on
   the keyserver using your email address, otherwise your key will only be
   searchable using its full fingerprint.

1. The new key should now be listed in the keys tab.

1. Now that your new OpenPGP key is created, make sure to [register it in the
   BioMedIT portal]({{< ref "#section-register-key" >}}).

### Generate a new key pair (CLI)

To generate a new key pair using _sett_ command line interface:

```shell
sett keys generate --name "Alice Smith" --email alice.smith@example.com
```

{{% alert title="Important" color="warning" %}}

During the key generation process, _sett_ will ask for a pass phrase (password)
that will protect the private key. Please use a reasonably strong password and
make sure to save it in a password manager.

**If the password is forgotten, there is no way to retrieve or re-create it**.
Your OpenPGP key pair will thus become unusable and will need to be revoked.

{{% /alert %}}

After creating the new key, _sett_ will display a revocation signature for it.
Make sure you keep it in a safe place, such as a password manager. Revocation
signatures can optionally be automatically exported to a file by using the
`--rev-sig` option:

```shell
sett keys generate --name "Alice Smith" --email alice.smith@example.com --rev-sig alice.rev
```

## List keys

### List keys (GUI)

Go to the **Keys** tab.

### List keys (CLI)

Use the `keys list` subcommand.

```shell
sett keys list
```

## Export/import private keys

In some situations (e.g., new computer setup, remote _sett_ environment) you
might need to copy or move your private key to a different machine. This can be
done by **exporting the private key** to a file, transferring the file to the
new machine, and **importing** it.

### Export key (GUI)

Click on the `...` icon next to the key you want to export and select
"Export public and private key".

### Export key (Cli)

Use the `keys export` subcommand:

```shell
# The key identifier can be its fingerprint, key ID or the user's email address.
sett keys export -o private_key.pgp -p alice.smith@example.com
```

Note that keys can also be exported in ASCII format (instead of binary) by
adding the `-a, --armor` option.

### Import key (GUI)

Click on the `+ Add` button and select "Import from file or clipboard".

### Import key (CLI)

Use the `keys import` subcommand.

```shell
sett keys import from-file -p private_key.pgp
```

Note the usage of the `-p` option to import both the private and public parts of
the key. If `-p` is omitted, only the public part of the key is imported.

Verify that the key has been correctly imported with `sett keys list`.

Ensure that you store any backed-up secret keys in a secure location and in an
encrypted form (typically in a password manager).

## Upload public key to the keyserver {#section-upload-key}

_sett_ allows users to upload their public OpenPGP key to a keyserver.

{{% alert title="BioMedIT" color="info" %}}

_sett_ uploads keys to the [OpenPGP keyserver](https://keys.openpgp.org).

[keys.openpgp.org](https://keys.openpgp.org) is a public keyserver for the
distribution and discovery of public PGP keys. This service is GDPR-compliant,
based on the open source software
[Hagrid](https://gitlab.com/hagrid-keyserver/hagrid), and already used by over
500'000 users worldwide. For more information about the service, please see
[this link](https://keys.openpgp.org/about).

{{% /alert %}}

### Upload public key to the keyserver (GUI)

1. Click on the `...` icon next to the key you want to upload and
   select "Upload public key to keyserver".
1. A dialog box will appear to ask for confirmation.
1. Make sure to check the checkbox to associate the key
   with your email address. To establish this association, the keyserver will
   send an email to verify that you are the legitimate owner of the email associated
   with the key.
   It also means that other people will now be able to search for your key on
   the keyserver using your email address, otherwise your key will only be
   searchable using its full fingerprint.
1. Click **Upload to keyserver**.
1. If you have selected the "associate key with email" checkbox, **you will
   shortly receive an email** from <keyserver@keys.openpgp.org> that contains a
   confirmation link to prove that you have access to your email.
1. Open the email and **click on the link** to associate your key with your
   email address. Other users now can find your public key with your email
   address.

### Upload public key to the keyserver (CLI)

Use the `keys upload` subcommand.

```shell
sett keys upload alice.smith@exaple.com
```

In order to also trigger email verification for your key, add the `-v, --verify`
option:

```shell
sett keys upload --verify alice.smith@exaple.com
```

## Register public OpenPGP key in the BioMedIT portal {#section-register-key}

{{% alert title="BioMedIT" color="info" %}}

**Registering your public OpenPGP key in the** [BioMedIT
portal](https://portal.dcc.sib.swiss) **is mandatory** to be able to use it
within the BioMedIT project (e.g. create data transfer requests, encrypt data,
etc.).

{{% /alert %}}

If you are _not_ a BioMedIT user, this section is not relevant for you and can
be skipped.

### BioMedIT OpenPGP key status {#biomedit-key-status}

OpenPGP keys used to encrypt, sign, and decrypt data within the BioMedIT
network require the approval of the **BioMedIT key validation authority**. The
information of whether a key is trusted or not is stored as **key status** in
the [BioMedIT portal](https://portal.dcc.sib.swiss). This is the reason why all
OpenPGP keys used within BioMedIT must be registered with the BioMedIT portal.

When an OpenPGP key is first registered in the BioMedIT portal, its status is
initially set to `PENDING` (i.e. it is awaiting approval). A key must have the
`APPROVED` status before it can be used to encrypt or sign data packages
within the BioMedIT network.

The list of key statuses is as follows:

- `PENDING`: a key approval request was submitted, but the key has not been
  approved yet. This is a manual process and can take from a few hours or up
  couple of days.
- `APPROVED`: key is approved for usage within the BioMedIT network. Only
  approved keys can be used to encrypt, sign, and decrypt data packages
  within the BioMedIT network.
- `APPROVAL-REVOKED`: approval of the key has been revoked by the BioMedIT key
  validation authority.
- `KEY-REVOKED`: key has been revoked by its owner.
- `REJECTED`: key is not trusted by the BioMedIT key validation authority.
- `DELETED`: key has been removed from the keyserver by its owner.
- `UNKNOWN KEY`: key has not been registered on the BioMedIT portal. If it is
  your own key, please register it. If it is the key of someone else, please ask
  them to register their key.

To verify that a key is trusted, _sett_ connects to the BioMedIT portal and
retrieves the status of the key. For this reason, it is important that BioMedIT
users **register their OpenPGP key with the BioMedIT portal**.

In cases where _sett_ is used outside of the BioMedIT project, or the portal is
not reachable, _sett_ can still be used to encrypt, decrypt, and transfer data.
In this case, you need to uncheck `Verify package` in the **Settings** tab.

The status of a public key in the BioMedIT portal can be easily checked in
GUI by going to the **Keys** tab and clicking on one of the keys.
Approved keys will also have a green "Approved" badge.

### How to register your public OpenPGP key in the BioMedIT portal?

{{% alert title="Only one active key" color="warning" %}}

Each BioMedIT user can only have **1 active OpenPGP key registered** in the
[BioMedIT portal](https://portal.dcc.sib.swiss) at any time.

If you wish to replace your currently active key with another one, please
connect to the [BioMedIT portal](https://portal.dcc.sib.swiss), go to the
**Profile** / **OpenPGP Keys** tab and click on the **"Retire Key"** icon
(**Actions** column) associated with your key.

{{% /alert %}}

{{% alert title="Key email must match user email" color="warning" %}}

The **email** associated with your OpenPGP key **must be one of the emails
associated to the SWITCH edu-ID account that you are using with the BioMedIT
portal**.

If the email associated to your OpenPGP key is different (e.g. a shared service
email is used for your OpenPGP key), please contact the BioMedIT support.

{{% /alert %}}

To register a new OpenPGP key with the BioMedIT portal, proceed as follows:

1. **Make sure you have successfully uploaded your key to the keyserver** and
   that you have **completed the email verification** procedure with the
   keyserver (i.e. your key must be _verified_ with the keyserver).

1. To make sure that your key is present on the keyserver and is verified, go to
   the [keyserver home page](https://keys.openpgp.org) in your browser and
   **search for the email associated with your key**. You should get a message
   saying that a key was found for your email address and the corresponding
   key fingerprint should be displayed.

1. **Copy the fingerprint** (40-character string) of your key.

1. **Log in** to the [BioMedIT portal](https://portal.dcc.sib.swiss).

1. Go to the **Profile** / **OpenPGP Keys**.

1. Click on the **"+ OPENPGP KEY"** button, a dialog box will open.

   _Note_: if the button is missing, it is probably because you already have an
   active key in the portal. Each user can only have 1 active key at a time -
   see the information box above.

1. Enter your full key fingerprint (must be exactly 40 characters long) in the
   dialog box, then press the green search icon to the right.

   This will retrieve the **User ID** and **Email** address associated with the
   fingerprint from the keyserver and display them in the dialog box.

1. Verify the user ID and mail address. If they are correct for your OpenPGP
   key, then click on **Confirm**.

1. A request to approve your key has now been sent to the BioMedIT key
   validation authority. Generally requests are processed quickly (in a matter
   of hours), but occasionally it might take slightly longer as this is a manual
   process.

   Please contact the BioMedIT support if your key has not been approved after a
   couple of days.

## Download public keys from the keyserver {#download-from-keyserver}

In order to encrypt data for a specific recipient (who will be able to decrypt
it), you will need to have the public OpenPGP key of that recipient(s) available
in your local keyring.

### Download public keys (GUI)

To download a public PGP key from the keyserver:

1. In the **Keys** tab, click on **+ Add**. From the drop-down menu, choose
   **Import from keyserver**. A dialog box will open, allowing to search for
   public keys stored on the keyserver.

1. In the search field, enter either the full email address or fingerprint of
   the public key you are looking for and click **Import key**.

   For instance, if searching for the key "Bob Test Key <bob@example.com>" with
   fingerprint "AEED7A96F339881F6FE8291464A1E0150613807D", one can search for
   either "<bob@example.com>" or "AEED7A96F339881F6FE8291464A1E0150613807D".

1. If the key you are looking for was found, you will see a success message
   with key details.

### Download public keys (CLI)

The `keys import from-keyserver` subcommand can be used to download public OpenPGP
keys from the keyserver.

```shell
sett keys import from-keyserver alice.smith@example.com
```

## Remove your public keys from the keyserver

While it is not possible to remove an actual key from the keyserver, it is
possible to remove all personally identifiable information from it
(user ID and email). Such keys are called **unverified**.

To remove personal information associated with a key, go to the keyserver's
[manage key page](https://keys.openpgp.org/manage), enter the email associated
to the key and click on **Send link**.

You will receive an email with further instructions on how to proceed to remove
your key's user ID and email from the keyserver.

{{% alert title="Warning" color="warning" %}}

Removing a key's user ID and email from the keyserver makes it no longer
searchable by email. It remains searchable by its full fingerprint.

**Important: keys without user ID and email cannot be used within the BioMedIT
network. Only remove your personal identifying information from a key if you are
no longer using it.**

{{% /alert %}}

## Delete keys from your local machine

Users are strongly discouraged from deleting keys from their local keystore.
Instead of deleting a key, users should **revoke** it. Revoking a key informs
others that the key should not be used anymore, and is a safer way to handle
unused or compromised keys.

Deleting private keys is possible from sett.

- GUI: Click on the `...` icon next to the key you want to delete and select "Delete".
- CLI: Use the `keys delete-private-key` subcommand.

  ```shell
  sett keys delete-private-key alice.smith@exaple.com
  ```

Deleting public keys is not possible with _sett_.
However, it is possible to delete a public key by manually deleting
the file containing the keys using your file explorer
or a shell command.

In the local _sett_ public keystore, keys are stored as files.
They are named after the fingerprint of the key's primary key.
For instance, a key with fingerprint
`3b17f529665fe012ef54f4a1714fdf98b6e828df` would be stored under:

- Public key:
  `<keystore-path>/3b/17f529665fe012ef54f4a1714fdf98b6e828df`

The location of the keystore (`<keystore-path>` above) is operating-system
dependent:

- **Linux:** `~/.local/share/pgp.cert.d`
- **Windows:** `%UserProfile%\AppData\Roaming\pgp.cert.d`
- **MacOS:** `~/Library/Application Support/pgp.cert.d`

You can also get the exact location of the key in sett GUI:

1. Click on the `...` icon next to the key you want to delete and select "Delete".
1. A dialog box with important information about key deletion, as well as the
   exact location of the key will be displayed.

## Generate a revocation signature {#generate-a-revocation-signature}

A prerequisite for revoking an OpenPGP key is to have generated a revocation
signature for it. If the OpenPGP key to revoke was generated with _sett_, you should
already have a revocation signature ready to use. If you do not
have a revocation signature yet, you can generate one with GUI:

1. Click on the `...` icon next to the key you want to generate revocation
   signature and select "Create revocation signature".
1. Fill-in the required fields:

   - **Reason**: the reason for revoking the key.
   - **Message**: a short explanation to the reason for revoking the key.
   - **Password**: the password for your private key.

1. Click **Generate signature**.
1. A new pop-up window will appear to confirm that the revocation signature was
   generated successfully. Copy the revocation signature to a safe location,
   ideally a password manager (anyone with access to a revocation signature can
   revoke the key for which it was generated).

## Revoke your key {#revoke-key}

If a private OpenPGP key has been compromised, is no longer usable (e.g.
password is lost), or should no longer be used for any other reason,
it must be revoked.

A prerequisite for revoking a PGP key is to have generated a [revocation
signature]({{< ref "#revocation-signatures" >}}) for it. If the OpenPGP key to
revoke was generated with _sett_, you should already have a revocation
signature ready to use. If you do not have a revocation signature yet, please
generate one by referring to the [Generate a revocation
signature]({{< ref "#generate-a-revocation-signature" >}}) section.

{{% alert title="Warning" color="warning" %}}

**A revoked key cannot be "un-revoked"**. Only revoke a key if you are certain
you want to do so. Revoked keys can no longer be used to encrypt/decrypt data
with _sett_.

{{% /alert %}}

### Revoke your key (GUI)

1. Click on the `...` icon next to the key you want to revoke
   and select "Revoke".
1. Either paste you revocation signature from the clipboard or load it from a
   file.

   **Warning:** proceed with caution, **a revoked key cannot be "un-revoked"**.

1. Click on **Revoke key**.

1. When expanding the revoked key from the **List of available keys**, you
   should now see the **Revocation status** as "Revoked". From this point on,
   the key can no longer be used with _sett_.

1. If you have previously shared your key via a keyserver (e.g.
   [keys.openpgp.org](https://keys.openpgp.org)), you must also re-upload your
   revoked key to that keyserver.

   This will allow other users to update their local copy of your public key,
   informing them that it is no longer valid. To upload your revoked public key,
   please refer to the
   [Upload your public PGP key to the keyserver ]({{< ref "#section-upload-key" >}}).

   If your key was never present on any keyserver, this step should be skipped.

### Revoke your key (CLI)

Use the `keys revoke` subcommand.

```shell
sett keys revoke alice.smith@example.com compromised "My dog ate it"
```

After a key has been revoked, it must be uploaded again to any keyserver(s)
where it is present, so that the revocation can be shared with others. This can
be done with _sett_ as illustrated in the [Upload your public PGP key to the
keyserver]({{< ref "#section-upload-key" >}}).

## Update expiration date for your key {#set-expiration-date}

It is possible to set an expiration date for an OpenPGP key. This is useful to
ensure that the key is not used indefinitely. It is possible to extend the
expiration date at any time.

### Update expiration date (GUI)

sett GUI warns you when your key is about to expire. On the **Keys** tab, you
will notice an _Expiring_ badge starting from three months before the expiration
date. On top of that, a warning will be displayed every time you use that key
for signing or decryption.

To set a new expiration date for your key from the GUI:

1. Click on the `...` icon next to the key you want to update the expiration
   date for.

1. Click on **Update expiration date**.

1. Choose a new expiry date.

1. Click on **Set new expiry date**.

1. Unlock your key by entering the password, if needed.

### Update expiration date (CLI)

sett CLI warns you every time you use a key which is about to expire for
signing or decryption. This happens starting from three months before the
expiration date.

To set a new expiration date for your key from the CLI:

```shell
sett keys expire alice.smith@example.com --expiration 3y
```

For details about the supported time specifications for the `--expiration`
option, run:

```shell
sett keys expire --help
```

## Migrating keys from the GnuPG keyring to the _sett_ keystore {#section-migrate-keys}

Public OpenPGP keys located in your GnuPG keyring are not automatically detected
by _sett_, and they must be migrated to _sett_'s public keystore. In contrast,
private keys present in GnuPG can be used in _sett_ without migration
(however, it is still possible to migrate them).

Note that to migrate public keys (e.g., keys from other people), you can simply
re-download them from the keyserver as shown in the [Download public PGP keys
from the keyserver]({{< ref "#download-from-keyserver" >}}) section.

- GUI:
  1. In the **Keys** tab, click on **+ Add**. From the drop-down menu, choose
     **Import from GnuPG**. A dialog box will open, containing a list of keys in
     your GnuPG keyring.
  1. Look for the correct key and click on **Import**. You should now see the key
     listed in the **List of available keys**. Note that if you are importing a
     private key, you will be asked to enter its password.
- CLI: use the `keys import from-gpg` subcommand.

  ```shell
  # The search term can be an email or fingerprint.
  sett keys import from-gpg alice.smith@example.com
  ```

## Introduction to public-key cryptography and OpenPGP {#intro_to_pgp}

Public-key cryptography is a method for secure communication between two or more
users. In this system, each user has a pair of unique keys consisting of a
**private key** and a **public key**. Public and private keys are linked in the
sense that, data encrypted with a given public key can only be decrypted with
the matching private key, and data signed with a given private key will only be
recognized by the matching public key.

Because these keys are based on the [OpenPGP](https://www.openpgp.org) protocol,
they will here be referred to as **OpenPGP keys**.

Public and private OpenPGP keys:

- **Public keys** are used to encrypt data, as well as for verifying signatures
  made on files or emails. By design, public keys are intended to be shared with
  other people and therefore no particular effort is required to keep them
  secret. In fact, public keys are often uploaded to public servers, known as
  **keyservers**, where they are accessible to anyone. No password is required
  to use a public key.

  Typically, public keys are used by data senders to encrypt data for one or
  more recipient(s), and by data recipients to verify signatures of files or
  emails (to ensure the sender is genuine).

- **Private keys**, sometimes also referred to as a **secret keys**, are used to
  decrypt data, sign files and sign other people's public keys. To increase
  security, private keys should always be password protected.

{{% alert title="Important" color="warning" %}}

**Private keys and their password must be kept secret at all times. Never share
your private key or password with anyone.**

Private keys should be stored in a directory only accessible by the key owner,
and their password should be stored in a password manager.

{{% /alert %}}

Typically, private keys are used by data recipients to decrypt data, and by data
senders to sign the files they encrypt.

{{% alert title="Important" color="warning" %}}

If anyone else than the legitimate owner has access to a private key and/or its
password, **the key pair is considered compromised**. It [must be
revoked]({{< ref "#revocation-signatures" >}}), and never used again.

{{% /alert %}}

_sett_ uses the open source implementation of public-key cryptography provided
by [Sequoia-PGP](https://sequoia-pgp.org/): a modular OpenPGP implementation in
Rust.

It is possible - and often desirable - to both encrypt and sign a file.
This ensures that the data can only be read by the intended recipient, and that
the recipient can be confident the sender is legitimate. This is precisely what
_sett_ does:

- **Encrypting files**, so that only the intended recipient(s) can read them.
- **Signing files**, so that the recipient(s) can trust the sender is genuine.

### Key fingerprints

Each pair of public/private OpenPGP keys is identified by a unique **fingerprint**.
Fingerprints are 40 characters long hexadecimal strings (digits and upper case
A-F letters) that look like this:

```text
238565936FCFF3F200219990941A3EC20555F781
```

Since nothing is preventing two OpenPGP keys to have the same user name and email
address, it is critical that users **always verify the genuineness of new keys**
before (or just after) importing them into their local keyring (i.e. their local
OpenPGP key database).

Ensuring a key is genuine can be done in two different ways:

- Ask the key owner to **provide their key's fingerprint via a trusted
  communication channel** (e.g. over the phone), and then verify that the
  fingerprint of the newly imported key does indeed match the fingerprint
  communicated to you by its owner.
- Using _sett-gui_, verify that the [key status]({{< ref "#biomedit-key-status" >}})
  of the key is **APPROVED** (can only be checked after you imported the key).

### File encryption

In public key cryptography, the sender **encrypts** a file using one or more
recipient(s) public key(s). Once a file is encrypted, no one can read the file
without having access to a private key that matches the public key(s) used for
encryption. This ensures that only the intended recipient(s) can decrypt the
file, because they are the only one to have access to the matching private key.

### File signing

The objective of **file signing** is to guarantee to the recipient of a file (or
email) that the sender is genuine, and not someone else trying to impersonate
the sender.

To achieve this, the sender signs the file with their private key (password
required), and shares their public key with the recipient (typically via a
keyserver). The recipient can then validate the authenticity of the signature
using the public key of the sender. Since public keys are non-sensitive, they
can be distributed publicly. In fact they are intended for this purpose, hence
their name.

### Revocation signatures

In the unfortunate event that a user either i) forgets their private key's
password or ii) have their private key and password stolen/compromised, they
will need a way to let other people know that their public key should no longer
be trusted and used.

This is because:

- If the password was forgotten: the key owner won't be able to decrypt data
  anymore.
- If the private key was compromised: someone else might be able to decrypt data
  encrypted with the public key, and to illegitimately sign files!

This situation is what **revocation signatures** are for: by applying a
revocation signature to a public key, and then sharing the revoked key with
others (e.g. via a keyserver), the key owner signals that their key is now
"revoked" and should no longer be trusted nor used. After a key has been
revoked, it can no longer be used to encrypt/decrypt data with _sett_.

Revocation signatures can be generated at anytime from the private key, but the
best practice is to generate them directly after a new key pair is created. This
ensures that the revocation signature will be available even if the private key
or its password is lost.

{{% alert title="Note" color="info" %}}

_sett_ will automatically generate a revocation signature each time a new
key is created.

{{% /alert %}}

Since anyone with access to a revocation signature will be able to revoke the
associated key, revocation signatures must be stored securely - e.g. in a
password manager - and should never be shared with anyone.

{{% alert title="Important" color="warning" %}}

- Revocation signatures **must be stored in a safe location** (e.g. a password
  manager) and **never shared with anyone**.
- It is best to generate a revocation signature **immediately after** a new
  public/private key pair is created.
- The key revocation process is described in
  [Revoke your OpenPGP key](#revoke-key) section.

{{% /alert %}}

### Exchanging public keys via a keyserver

Encrypting files for a specific recipient requires to have the recipient's
public key in one's local **keyring** (a keyring is a local database containing
OpenPGP keys). Similarly, verifying a signature on a file or a public key requires
to have the signee's public key available in one's local keyring.

Public keys are not sensitive data, and therefore can be sent
unencrypted via email. However, when having frequent key exchanges between
multiple actors, sending public OpenPGP keys around by email quickly becomes
cumbersome. A solution to this problem is using a so called **keyserver** to
share public keys. Keyservers are public or private servers whose sole purpose
is to store public OpenPGP keys and allow users to search for them.

{{% alert title="BioMedIT" color="info" %}}

Within the BioMedIT project, **all exchanges of public keys are done via the
public keyserver at** <https://keys.openpgp.org>. This keyserver can be accessed
directly from _sett_ or via a web browser.

{{% /alert %}}
