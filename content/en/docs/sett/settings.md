---
title: "Settings"
linkTitle: "Settings"
weight: 80
---

## GUI settings {#gui}

The _sett_ desktop app allows a number of options to be customized via its
**Settings** ⚙️ page. For instance, you may change the default output directory,
or enable/disable package verification before a transfer.

Each setting has a predefined default value, which is used when first running
the tool or if loading the current settings fails for any reasons.

Changes made to **Settings** become effective immediately. Changes can be reset
back to their factory default by clicking on the **Reset settings** button.

Settings are divided into three sections: "basic", "advanced", and "non-editable".

### Basic

Verify package

: When enabled (the default value), the following checks are made before
encrypting or transferring data:

- **DTR ID** is valid and the transfer is authorized.
- **Sender** and **Recipients** public OpenPGP keys are approved by the
  BioMedIT key validation authority.
- **Recipients** are approved _Data Managers_ of the BioMedIT project for which
  data is being encrypted.
- The name of the data package matches the pattern
  `<project_code>_<date_format>.zip`. This ensures no sensitive information
  is mistakenly included in the file name.

Note that that some of the above checks require communication with the
**BioMedIT portal**. When using _sett_ outside of a BioMedIT project, this
setting should therefore be disabled.

Default output directory

: Default destination directory for operations such as encryption to the local
filesystem or decryption. User's home directory is used by default.

### Advanced

Enable extra metadata

: When enabled, additional metadata can be added to the package in the
data encryption form.

OIDC issuer URL

: URL of the **OpenID Connect** issuer used for authentication
(BioMedIT specific).

Portal URL

: URL of a **BioMedIT portal** instance. **Portal** is used for key
approval verification, **DTR** (Data Transfer Request) validation,
and retrieval of data associated with a given DTR
(when _sett_ is being used in authenticated mode).
The default value of this setting is: <https://portal.dcc.sib.swiss>.

Public key store

: Directory where public OpenPGP keys are stored.

Private key store

: Directory where private OpenPGP keys are stored.

### Non-editable

This section displays values of setting that cannot be modified by the user.
These settings are displayed here for convenience. They can be copied to the
clipboard via a dedicated "copy to clipboard" button.

Log directory

: Directory where log files are stored. Location of the log directory depends
on the operating system:

- Linux: `${XDG_DATA_HOME}/ch.biomedit.sett/log` or `$HOME/.local/share/ch.biomedit.sett/log`
- macOS: `$HOME/Library/Application Support/ch.biomedit.sett/log`
- Windows: `{FOLDERID_RoamingAppData}\ch.biomedit.sett\log`

Keyserver URL

: URL of the OpenPGP key server used to retrieve and publish public keys.
The default value is <https://keys.openpgp.org>.

## CLI settings {#cli}

The _sett-cli_ is **stateless** by design, meaning that there is no persistent
configuration file where settings can be modified and stored.  
Instead, settings can be set via the following shell
**environment variables**. All settings are optional and have a default value.

`SETT_OPENPGP_KEY_PWD`

: Password to unlock the secret OpenPGP key used to decrypt or sign data. When
this environmental variable is set, _sett_ uses its content instead of
interactively asking the user to enter a password.

`SETT_OPENPGP_KEY_PWD_FILE`

: Full path and name of a file containing the password to unlock the secret
OpenPGP key used to decrypt or sign data. When this environmental variable is
set, _sett_ uses its content instead of interactively asking the user to enter
a password. The file containing the password should not be encrypted.

`SETT_PORTAL_URL`

: URL of the **BioMedIT Portal** instance to be used. For details see the
description of the GUI **Portal URL** setting.
This setting defaults to <https://portal.dcc.sib.swiss>.

`SETT_OIDC_CLIENT_ID`

: Client ID with which _sett_ should identify with the OpenID Connect issuer
(see `SETT_OIDC_ISSUER_URL`). Only relevant when using _sett_ in authenticated
mode. This setting defaults to `sett`.

`SETT_OIDC_ISSUER_URL`

: URL of the OpenID Connect issuer used when authenticating with the
**BioMedIT Portal**. Only relevant when using _sett_ in authenticated
mode.
This setting defaults to <https://login.biomedit.ch/realms/biomedit>.

`SETT_KEYSTORE`

: Directory where private OpenPGP keys are stored. Location is platform dependent:

- Linux: `${XDG_DATA_HOME}/sequoia/keystore` or `$HOME/.local/share/sequoia/keystore`
- MacOS: `$HOME/Library/Application Support/org.Sequoia-PGP.sequoia/keystore`
- Windows: `{FOLDERID_RoamingAppData}\org.Sequoia-PGP.sequoia\keystore`

`PGP_CERT_D`

: Directory where public OpenPGP keys are stored. Location is platform dependent:

- Linux: `${XDG_DATA_HOME}/pgp.cert.d` or `$HOME/.local/share/pgp.cert.d`
- MacOS: `$HOME/Library/Application Support/pgp.cert.d`
- Windows: `{FOLDERID_RoamingAppData}\pgp.cert.d`

`SETT_METADATA_EXTRA`

: Additional metadata to be included in the data package.
Extra fields must be provided in the form of key=value pairs separated by a
comma and without spaces.
For example: `SETT_METADATA_EXTRA="foo='value 1',bar=value_2"`
