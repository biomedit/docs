---
title: "FAQs"
linkTitle: "FAQs"
hide_summary: true
menu:
  main:
    weight: 30
type: docs
---

### 1. General Information

---

<details> <summary><B>1.1 What is BioMedIT?</B></summary>

In 2017, the Swiss Confederation launched the BioMedIT network project, as
integral part of [SPHN](https://sphn.ch/). A project
of [SIB](http://www.sib.swiss/) Swiss Institute of Bioinformatics, the secure
and cutting-edge IT environment BioMedIT is established nationally to support
computational, biomedical research and clinical bioinformatics, ensuring data
privacy. Given the sensitive nature of health-related information, research
using patient data imposes high demands on IT infrastructures and processes to
fulfil the stringent legal and ethical requirements. The goal of SIB’s BioMedIT
project is to build and maintain a secure and protected IT environment,
providing researchers with access to a cutting edge research IT infrastructure
for analysis of sensitive data, without compromising data privacy. BioMedIT is a
national infrastructure resource that can jointly be used by all Swiss
Universities, research institutions, hospitals and other interested partners.

You may find more information at [https://biomedit.ch](https://biomedit.ch)

</details>

<details>
<summary><B>1.2 What is the BioMedIT network?</B></summary>

The BioMedIT Network builds on three legally independent scientific IT
competence platforms:

- [sciCORE](https://scicore.unibas.ch/) in Basel, operated by the University of
  Basel,
- [Unil / SIB](https://sensa.sib.swiss/) in Lausanne, operated in collaboration
  by SIB and Unil, and
- [SIS](https://sis.id.ethz.ch/) in Zurich, operated by ETHZ.

Under the umbrella of the BioMedIT Network Project, all three engaged
institutions committed to build a high performance computing infrastructure (in
addition to their already existing scientific compute clusters) especially
designed for sensitive (confidential) data for Personalized Health and
data-driven research: sciCOREmed in Basel, SENSA (Secure sENSitive data
processing plAtform) in Lausanne, and Leonhard Med in Zurich – the BioMedIT
nodes.

Central infrastructure components (such as tools, platforms, etc.) and
procedural solutions are under the responsibility of SIB’s [PHI
Group](https://sphn.ch/organization/implementation-teams/). PHI operates a
central service layer and is responsible for the coordination of the BioMedIT
Network Project.

</details>

<details><summary><B>1.3 What are "Nodes"?</B></summary>

A BioMedIT node is a local or regional node that provides a secure compute and
storage infrastructure for handling (securely storing, managing and processing)
sensitive research data, whether clear text, pseudonymized or coded personal
data. They are the entry and exit points for data transfers between data
providers and recipients. Each data provider is associated with a single node
and all data from that provider passes through the node.

The node may be a _destination_ node for projects hosted on the node or it may
be a _transfer_ node for projects hosted on another node.

More information about the Nodes including contact details may be found on
[https://biomedit.ch](https://biomedit.ch).

</details>

<details><summary><B>1.4 How do I access the BioMedIT network?</B></summary>

Access for almost all operations on the BioMedIT network is through the
BioMedIT portal <https://portal.dcc.sib.swiss>. The BioMedIT
website [https://biomedit.ch](https://biomedit.ch) provides additional details
of all resources supported on BioMedIT.

The access to the BioMedIT Portal requires a SWITCH edu-ID protected by two
factor authentication (2FA) with a validated email address from a Swiss
university, hospital or other similar institution. Sending data from a Data
Provider into the BioMedIT Network is covered in the FAQ sections [Data
Providers](#2-data-providers) and [Data Transfers](#4-data-transfers).

</details>

<details><summary><B>1.5 Why should I use the BioMedIT network for my data transfers?</B></summary>

Confidential data should stay confidential. Sending it to another person by
attaching a file to an email or putting it on a USB stick is very risky. Data
which is confidential should be encrypted and sent by secure means. BioMedIT has
been set up to do just that. A Data Provider connects securely to a single
contact point in the BioMedIT network (irrespective of the intended final
destination of the data). From this contact point data can be securely routed
anywhere inside the BioMedIT network to Universities, University Hospitals and
other institutions in Switzerland. Encryption of data, secure transfer
protocols, monitoring and logging of the transfer process, management of the
keys used for encryption and decryption, and strict control of user access and
rights on the network significantly enhance the security of the transfer of
sensitive data.

A secure encryption and transfer tool, [sett](/docs/sett), has been developed to
make the transfer process as easy as possible for all users of the BioMedIT
network.

</details>

<details> <summary><B>1.6 My project receives data from outside Switzerland. Can
I use BioMedIT?</B></summary>

A pre-requisite for any data sent from a data provider into the BioMedIT network
is that appropriate ethical and legal approvals exist for the data proposed to
be sent. The question of compliance with the GDPR requirements must be settled
at this stage if the data set will contain records from EU nationals. Similarly
HIPAA requirements might need to be met if the data set will contain records
from US nationals. If these ethical, legal and privacy considerations are met
the remaining questions are mainly technical and administrative. The project
staff should discuss with the DCC and relevant BioMedIT node how the data
transfer and management process can best be handled.

</details>

<details>
<summary><B>1.7 Who pays for all this?</B></summary>

Based on the Funding Principles and Funding Regulations, the Swiss Government
allocated around CHF 18 million to BioMedIT for the first period 2017-2020.
Funding continues for the second phase of the project which runs from 2021 until
the end of 2024.

The project is under the responsibility of [SIB](http://www.sib.swiss/) Swiss
Institute of Bioinformatics. The matching funds' principle applies to all
financial contributions, i.e. the participating institutions must provide their
own contributions (in cash and/or in-kind) to match the funds provided
by BioMedIT.

</details>

<details><summary><B>1.8 Where can I read more about how BioMedIT works?</B></summary>

The BioMedIT website at [https://biomedit.ch](https://biomedit.ch) and the [SPHN
website](https://sphn.ch/) contain detailed information about BioMedIT and its
resources. The documentation contained here and in our [DCC GIT
WIKI](https://git.dcc.sib.swiss/dcc/documentation-biomedit/-/wikis/home) provide
more detail on specific aspects of BioMedIT.

</details>

<details><summary><B>1.9 Who can I contact if I have more questions?</B></summary>

You may visit the BioMedIT website
at [https://biomedit.ch](https://biomedit.ch) or the SPHN website
at [https://sphn.ch](https://sphn.ch.).

For additional information write to <dcc@sib.swiss>.

For day to day operational issues or something specifically related to BioMedIT
write a mail to <biomedit@sib.swiss>. A ticket will be opened with your request
and you will be contacted by one of the BioMedIT support personnel.

</details>

<details><summary><B>1.10 Is there documentation available for ..?</B></summary>

A number of work instructions, user guides and the like are publicly available
in [this site](https://biomedit.gitlab.io/) and at
[https://biomedit.ch/](https://biomedit.ch).

</details>

### 2. Data Providers

---

<details><summary><B>2.1 What do I need to do to become a Data Provider?</B></summary>

The procedure for becoming a Data Provider to the BioMedIT network is described
in detail in the document [Onboarding of Data
Providers](https://git.dcc.sib.swiss/dcc/documentation-biomedit/-/wikis/WI-008-Onboarding-of-Data-Providers).
In summary there are two parts - administrative and technical. A prospective
Data Provider will contact a BioMedIT node, DCC or a project and submit a
request to DCC to be added as a new Data Provider. Technical (and possibly
administrative) contact details will be provided. The technical contact will
provide details about the data transfer system of the Data Provider and receive
information from the associated BioMedIT node about the server to be used for
connection to the BioMedIT network. More information can be found in the
document [Data Transfer for Data
Providers](https://git.dcc.sib.swiss/dcc/documentation-biomedit/-/wikis/WI-003-Data-Transfer-for-Data-Providers).
Before any patient data can be transferred from a Data Provider to the BioMedIT
network the legal basis must be confirmed. This will normally involve the
parties providing and receiving data agreeing to and signing a [Data Transfer
and Use Agreement (DTUA)](https://sphn.ch/services/dtua/). A DTUA is project
specific and describes the data to be transferred (including the format of the
data and meta-data) and the purpose for which it will be used. A DTUA will be
signed by the authorised representatives of the provider and recipient as well
as the project leaders.

</details>

<details><summary><B>2.2 What information do I need to provide?</B></summary>

Assuming that you have agreed with the BioMedIT node to which you wish to
connect that they will serve as your entry point into the BioMedIT network, you
will need to provide them:

1. List of IP addresses used as exit points to send data to the BioMedIT network
   (required for whitelisting in the BioMedIT nodes).
2. Expected data volume and frequency of transfers
3. The Data Provider’s public SSH key
4. Contact details of the persons nominated as administrative and technical
   contacts (name, email address and role)

Please refer to [Onboarding of Data
Providers](https://git.dcc.sib.swiss/dcc/documentation-biomedit/-/wikis/WI-008-Onboarding-of-Data-Providers)
for more information.

</details>

<details><summary><B>2.3 The project I am sending data to is hosted on Node X.
Why do I have to connect to Node Y and send the data there? Would it not be
simpler to send it directly to Node X?</B>

</summary>

This is a great question and one which is often asked! The answer is not
immediately obvious. If a hospital is taking part in a project hosted on Node X
then the easiest thing is to just connect directly to this Node. The problem
with the approach is scalability. With one project per data provider things are
simple. At present there are 3 BioMedIT nodes in Switzerland but this could very
well increase in future. Now the hospital (Data Provider) has to keep track of
multiple projects on multiple BioMedIT nodes and each Node has to separately
onboard each hospital. Each hospital would have a Landing Zone for data transfer
set up on each Node. Some projects are already being hosted on more than
one BioMedIT node so a hospital as a Data Provider would need to work out where
individual data sets should be sent - to one node, another node, both nodes..
Keeping track of all of these things, particularly in the future when other
hospitals, potentially also ones from outside Switzerland, are transferring data
to projects in the BioMedIT network would be a real problem. Logging and
monitoring of data transfers, necessary if a proper audit trail is to be kept,
would be quite complicated and become difficult to maintain an oversight.
Tracking down which package went where and from whom would be more complex than
necessary. The solution adopted on BioMedIT is to associate each Data Provider
with one BioMedIT node and to onboard the hospital with just that node.All data
transfers from a Data Provider are then sent to only that one Node, always the
same one, and hopefully the problem is solved.On BioMedIT this method of
connecting a data provider with a project has become known as the Snowflake
architecture - a data package "falls" into one node and is routed automatically
to the node or nodes where the project is hosted. It is the case that in the
past some SPHN driver projects have established data transfer paths directly to
nodes other than the 'main' BioMedIT node. Requests from future projects or Data
Provider to be connected to a specific node will be assessed on a case by case
basis.

</details>

<details><summary><B>2.4 Is there a tool I can use to help me transfer data?</B></summary>

There are three ways to set up and transfer data which are currently in use on
the BioMedIT network.

Most Data Providers are using the secure encryption and transfer tool,
[sett](/docs/sett), to manage the transfer.

While most use the GUI version there is also a command line version with
slightly reduced functionality (mainly in key management) but some additional
features such as facilitating automated transfers.Both versions provide some
degree of checking for valid Data Transfer Request IDs and that the recipient is
actually a Data Manager of the project.The third alternative, not to use sett
but a variety of individual tools, is not as popular. If this last option is
chosen careful attention must be paid to the correct construction of the data
package and metadata file as well as verifying DTR ID and recipient.

</details>

<details><summary><B>2.5 Why can I not send data into the BioMedIT Network?</B></summary>

There are a number of reasons why a data transfer might not work.

- As a Data Provider you need to be onboarded to a BioMedIT node. Done? See
  above for more information on onboarding.
- Do you have the most up to date version of sett? Occasionally an update of the
  portal system might require the use of the latest version of sett.
- If the encryption of a data package using sett fails:
  - Try the _Test_ function of the encryption section of sett. Does this work?
    Are there error messages?
  - If _Test_ worked then try _Package & Encrypt_. Does this work? Are there
    error messages?
- Possible problems: Do you (Data Provider) have valid GPG keys with the public
  key signed by the DCC and uploaded to the BioMedIT keyserver?
- There needs to be a valid DTUA (and possibly other agreements) in place
  between the Data Provider and the Project/Institution. Done?
- The sending of data into the BioMedIT network requires that an authorised Data
  Transfer Request (DTR) exists.
- The Data Manager of a Project creates the DTR and approval to send data
  requires confirmation of readiness (legal and technical).
- Is the DTR ID correct?
- Is the DTR authorised (there is a flag in the BioMedIT Portal which must be
  set to AUTHORIZED)?
- Did the Verify DTR ID work (box in Encrypt section of sett is checked by
  default, requires that the user's computer can access the internet)?
- To encrypt data a Data Provider requires the public key of the Data Manager
  which has been signed by the DCC. Is this public key valid and signed?
- The public key being used to encrypt the data must belong to a person who has
  been assigned the role of Data Manager of the project in the BioMedIT
  Portal/User Register. Is there an error message about the role of the
  recipient?
- Is the DTR for the project of which the person whose public key is being used
  is a Data Manager?
- If the encryption works but the transfer does not: Try the _Test_ function of
  the transfer section of sett. Does this work? Are there error messages?
- If _Test_ worked then try _Transfer selected files_. Does this work? Are there
  error messages?
- Did you have the IP addresses used by the Data Provider whitelisted by the
  BioMedIT node? (This is normally done as part of the on boarding process)

If none of the above seems to be a problem then please send a ticket to
the BioMedIT help-desk at [biomedit@sib.swiss](mailto:biomedit@sib.swiss)
stating the nature of the problem including error messages received (copy and
paste the message into the email or take a screenshot/photo).

</details>

<details>
<summary><B>2.6 My hospital does not have a clinical data warehouse. Can I still
send data into the BioMedIT network?</B></summary>

The information in the other answers in this section provide the background on
how to become an approved and onboarded data provider for projects on BioMedIT.
Provided your hospital can meet these requirements there is no reason why you
cannot send data into the BioMedIT network.

</details>

### 3. Projects

---

<details><summary><B>3.1. How do I set up a project on BioMedIT?</B></summary>

In principle any research project dealing with sensitive or confidential data
and needing to leverage a secure computing environment can use BioMedIT.
Typically the BioMedIT node which is hosting the project will act as a service
provider but there is no reason it cannot be a project partner as well. Projects
can, if desired, take advantage of the HPC facilities offered by the BioMedIT
nodes. Projects to be hosted on a BioMedIT node need to be onboarded. When this
is done and the project is listed on the BioMedIT Portal users who are
authorised to use the BioMedIT Network can be assigned a role on the project.
Where a project receives data from a provider which is already linked to and
onboarded at a BioMedIT node requests to transfer data can be made by the
project Data Manager. Where a new data provider is being used the data provider
must be onboarded to one of the BioMedIT nodes. To initiate the setup of the
project the Project Leader should contact the head of the nearest BioMedIT node
(SENSA in the Romandie, sciCORE in central Switzerland or SIS in the east/south
of Switzerland).

More information about the Nodes including contact details may be found on
<https://biomedit.ch>.

In all cases there are standard operating procedures (SOPs) and work
instructions (WIs) which deal with the requirements for a project and/or a data
provider to become part of the BioMedIT Network.

#### Requirements

A project goes through a process of administrative and technical onboarding
during the set-up phase. The main requirements for a successful onboarding are:
deciding where the project should be hosted (which BioMedIT node) agreeing on
the project requirements for: computing facilities storage remote access
archival of project data at completion of project establishing and agreeing a
Data Transfer and Use Agreement between the Data Providers, BioMedIT node(s) and
the project agreeing a Service Level Agreement agreeing the costing of the
hosting, technical support, user management and any other scientific support.
The project may at this time also discuss with the proposed hosting node what
will happen to the project data at the completion of the project, whether the
project work space is to be archived and stored for a certain length of time,
and access to this data/workspace after the project is formally complete.
Removal or maintenance of long term user access rights might also be discussed.
The associated costs to be met for providing these services should also be
agreed between the project and the BioMedIT hosting node.In the BioMedIT Portal
a portal administrator will create the project record and include details about
the Project Leader (PL), hosting node, resources to be made available, and (if
the PL opts out of accessing the portal/project work space) will assign to a
user (upon the request of the PL) the role of Permissions Manager for the
project.

</details>

<details><summary><B>3.2. What are the roles used on projects in the BioMedIT Network?</B></summary>

There are four roles which relate to persons associated with a project in the
BioMedIT Network:

- **Project Leader:** Overall responsibility for project - technical and
  administrative aspects of project hosting on one or more BioMedIT nodes, user
  management, data management.
- **Permissions Manager:** Carries out user management in the BioMedIT Portal
- **Data Manager:** Creates Data Transfer requests in the BioMedIT Portal and
  ensures that the necessary documentation and technical requirements are met
  for each data transfer from a data provider to the project.
- **User:** Accesses data in the project work space on BioMedIT.

</details>

<details><summary><B>3.3. What is the role of a Permissions Manager on a project?</B></summary>

The Permissions Manager (PM), accountable to the Project Leader, carries out all
the user management on the project. Each project must have at least one project
member with the user role of PM. The PM is responsible for assigning, modifying
and revoking the user roles of “(default) User” and “Data Manager” to authorised
BioMedIT users. A PM acts as contact for the new project members to support them
with registration and authorisation as users of BioMedIT.

</details>

<details><summary><B>3.4. How do I as Project Leader or Permissions Manager
assign users and roles to a project?</B></summary>

Only the Project Leader (PL) or the designated Permissions Manager(s) (PM) may
assign users to a project and specify the role(s) they can be allocated.While a
PL can assign a user as a PM, Data Manager (DM) or ordinary User the PM can only
assign a user as DM or User. A DM may not assign user roles. The
assigning/removing of users/roles to a project is carried out on the [BioMedIT
Portal](https://portal.dcc.sib.swiss) at any time. (Instructions can be found in
the [BioMedIT Portal User
Guide](https://xchange.dcc.sib.swiss/display/SPHNDCC/Projects#Projects-managing_project_users))

**Note:** Assigning or changing the role of _User_ will automatically trigger an
email to the BioMedIT node where the B-space is hosted. If the User role is
assigned the Node will create a local user account in the B-space. If the role
is removed the Node will remove access to the B-space for that user.

Before you (Permissions Manager) can assign a team member as a user of your
Project, each user, including the Permission Manager, needs to go through the
following steps:

- Create a SWITCH edu-ID account and enable the two factor authentication (2FA)
  (SWITCH homepage)
- Sign the AUP of the BioMedIT node where the project is being hosted (please
  get in contact with the BioMedIT node)
- Complete the [BioMedIT Security Awareness Training course](/docs/training/)
- Register at the [BioMedIT Portal](https://portal.dcc.sib.swiss) by logging in
  with your SWITCH edu-ID (and second factor authentication)

All additions or alterations will trigger automatic emails to the PL and DCC as
well as being logged in the BioMedIT Register system to provide an audit trail.

</details>

<details><summary><B>3.5. What is the role of a Data Manager on a Project?</B></summary>

The Data Manager (DM), accountable to the PL, manages all data transfers from
one or more data providers to the project.

Each project must have at least one project member with the user role of DM.

The DM is responsible for the management of the PGP cryptographic key pair used
for encrypting, decrypting and signing data packages.

The DM also generates the Data Transfer Requests in the BioMedIT Portal to
ensure that all administrative and technical matters relating to the transfer of
data have been positively addressed.

The DM unpacks and decrypts the data packages transferred from a data provider
(there may be multiple data providers for a single project) in the B-space on
the BioMedIT node where the project is hosted.

Both the project and the relevant data providers must have been previously
on-boarded to a BioMedIT node before any data may be transferred.

With the express authorisation from the PL the DM is responsible for the
packaging, encrypting and signing of data packages which are to be transferred
out of the B-space in the secure BioMedIT environment.

</details>

<details><summary><B>3.8. I am a project member of project X which has a B-space
on a BioMedIT node. How can I access this B-space?</B></summary>

Before you can access the workspace there are a number of things that need to be
done:

- You must become an authorised BioMedIT user.
- [Create an SWITCH eduID](https://eduid.ch/web/registration/) (using the email
  address from your institution) if you do not already have one and enable two
  factor authentication (2FA)
- Login to the [BioMedIT Portal](https://portal.dcc.sib.swiss) with this eduID
  and set a user name
- Complete the [Security Awareness training](/docs/training/)
- Send a request to the project Permissions Manager to be assigned a role on the
  project

When you are assigned a role on the project and you log into the BioMedIT Portal
you should see the project listed under Projects.

There will be one or more ways (referred to as Resources) in which you can
access the B-space (Graphical remote desktop or Command line in a terminal).

Note: access to a Gitlab instance for the project may also be shown as an
available resource.

</details>

<details><summary><B>3.9. Where is the project data stored?</B></summary>

#### > LeonhardMed at SIS (Zürich)

When you open a Remote Desktop session you should see files and folders in your
home folder (your username will be the name of the folder, (LeoMed at SIS
example) /cluster/home/dduck). There should also be a folder visible in the File
Manager with the same name as the project. This folder, accessible by all users
on the project, is where the project data is stored.This folder will be located
at (example for IMAGINE project hosted on LeoMed at SIS)
/cluster/dataset/imagine.There may also be a folder called (example for IMAGINE
project) /cluster/dataset/imagine but this is the same folder as
/cluster/dataset/imagine. If you see a folder (example for IMAGINE project) like
/cluster/dataset/imagine_lz this is the landing zone folder used by the Data
Manager when transferring data via the BioMedIT secure data transfer process.
You can find some more information here.

#### > sciCOREMed at sciCORE (Basel)

When data is transferred with sett, the landing zone is mounted from GPFS NFS to
/transfer. The data is then visible in the /transfer folder until it is
decrypted into the /project/data folder (on NetApp over NFS). Additionally the
user has access to the /project/home folder, which is on Ceph filesystem.

#### > SENSA / SIB (Lausanne)

When you open a Remote Desktop session you should see files and folders in your
home folder. The project folder is auto-mounted from the WEKA FS and should be
visible at login.

</details>

<details><summary><B>3.10. How do I become an authorised BioMedIT user?</B></summary>

To become an authorised BioMedIT user:

1. Create a [SWITCH eduID](/docs/switch-edu-id/switch-edu-id/) (using the email
   address from your institution) if you do not already have one, and enable two
   factor authentication (2FA).
2. Login to the [BioMedIT Portal](https://portal.dcc.sib.swiss) with this eduID
   and set a user name. At this stage you cannot yet be assigned to a project.
3. Complete the mandatory [BioMedIT Security Awareness
   Training](/docs/training/)
4. Sign your BioMedIT node's Acceptable Use Policy (AUP) and any other relevant
   documents from the project.

</details>

### 4\. Data Transfers

---

<details><summary><B>4.1. How do I (Data Manager of project) request a Data
Provider to transfer data?</B></summary>

Log into the BioMedIT Portal and follow the instructions detailed in the
[BioMedIT Portal User
Guide](/docs/portal/projects/#submitting-a-data-transfer-request)

</details>

<details><summary><B>4.2. I (Data Manager of Project) will have ongoing data
transfers to my project. Do I need to request each individual transfer?</B></summary>

After one successful production data transfer from data provider to the project
work space, a DTR which is marked as _AUTHORIZED_ and with _maximum number of
packages_ set as _unlimited_ may be re-used for future data transfers. A new
request will only be needed if either the legal basis or the technical setup
changes.

</details>

<details>
<summary><B>4.3. What information do I (Data Provider) need to receive from the
project to transfer data?</B></summary>

The main piece of information a DP needs is the verified and approved public PGP
key of the Data Manager of the project. In addition the DP needs to know the
Data Transfer Request ID (this information is available in the BioMedIT Portal).
The BioMedIT node will have already passed on the necessary information for
sending data (URL, username, directory) from the Data Provider to the BioMedIT
Network.

</details>

<details><summary><B>4.4. What are "PGP keys" and why do I need to have them?</B></summary>

Public-key cryptography is a method for secure communication between two or more
users. In this system, each user has a pair of unique keys consisting of a
private key and a public key. Public and private keys are linked in the sense
that, whatever has been encrypted with the a given public key can only be
decrypted with the matching private key, and whatever has been signed with a
given private key will only be recognized by the matching public key.

You can read more about key management, and how sett does most of the hard work
for you, in the documentation of sett on the [sett User Guide](/docs/sett)

</details>

</details>

<details><summary><B>4.5. How can I get my key authorized?</B></summary>

To get your public PGP key authorized, it must be registered in the BioMedIT
Portal. Instructions are available in this section of the [sett User
Guide](/docs/sett/key_management/#section-register-key).

</details>

<details><summary><B>4.6. Help! I lost the password to my private key. Can you
reset my password or send me another one?</B></summary>

No! A private key is just that - private. If you have lost the password to the
key it is gone forever.

You will need to revoke the previous key and generate new public and private PGP
keys. See [here](/docs/sett/key_management/#section-generate-key) for details.

If you generated the keys within sett a revocation certificate was generated
automatically for you.
See [here](/docs/sett/key_management/#revocation-certificates) for details.

</details>

<details><summary><B>4.7. I sent my private key in an email. What should I do?</B></summary>

If you think that the private key is no longer private you should [revoke the
key](/docs/sett/key_management/#revoke-your-pgp-key) and [generate new
keys](/docs/sett/key_management/#section-generate-key).

</details>

<details><summary><B>4.8. How does data get from the Data Provider to the
project working space on BioMedIT?</B></summary>

Once a Data Transfer Request is approved the Data Provider assembles the agreed
data set (selects records, carries out necessary de-identification and any other
preparation work required). The sett tool is then used to compress the data
(optional), encrypt it with the public key of the recipient, sign it with the
Data Provider's private key, and finally to transfer it to the BioMedIT node to
which the Project is connected. After the transfer completed, the package is
made available to the data recipient in the secure environment of the BioMedIT
node, and can be decrypted and unpacked in the recipient's B-space using _sett_.
The transfer takes place using what is often called end-to-end encryption
(encrypted before leaving the Data Provider and only decrypted after arriving in
the B-space). The transfer of the encrypted data package takes place over a
secure, encrypted path which further enhances security.

</details>

<details><summary><B>4.9. I am a Data Manager of a project. How can I track the
data transfer process?</B></summary>

When data is transferred the current status will be shown in the portal (number
of packages transferred and the path the packages take from the Data Provider to
the B-space). See [Data Transfer - LOGS](/docs/portal/data_transfers/#logs-tab)

</details>

<details><summary><B>4.10. Why do I receive all these mails from **biomedit@sib.swiss**?</B></summary>

The ticket system in use on BioMedIT uses the email
address [biomedit@sib.swiss](mailto:biomedit@sib.swiss.)

If you are receiving mails from this address it means that either you raised a
ticket for support or are part of a data transfer process between a Data
Provider and a Project.

Should you not wish to receive further emails from <biomedit@sib.swiss> in
relation to data transfers just reply to one of the mails requesting that you be
deleted from the list of correspondents.

</details>

<details><summary><B>4.11. What is sett?</B></summary>

**sett** stands for \"Secure Encryption and Transfer Tool\" and is an
application that facilitates and automates data packaging, encryption, and transfer.
It is written in Rust, a fast and memory-safe programming language.
**sett** is available both as a desktop and a command line application.

**sett** is developed as part of the [BioMedIT](https://www.biomedit.ch/)
project. It is licensed under the
[GPLv3 (GNU General Public License)](https://www.gnu.org/licenses/gpl-3.0)
and the source code is available from its public
[GitLab repository](https://gitlab.com/biomedit/sett-rs).

Fore more information, visit the [sett User Guide](/docs/sett/)

</details>

<details><summary><B>4.12. Where can I get more information about sett?</B></summary>

Full documentation for sett (including information on key management) may be
found at [sett User Guide](/docs/sett/).

</details>

<details><summary><B>4.13. I think I found a bug in the sett tool. How can I
report it?</B></summary>

Please open an issue on [GitLab](https://gitlab.com/biomedit/sett-rs/-/issues).

The developers monitor the issues on GitLab for any bugs, problems, or feature
requests.

</details>

<details><summary><B>4.14. I don't want to use this sett tool! How can I send
data to the project without it?</B></summary>

sett was developed at the request of a number of the data providers to
the BioMedIT network. The packaging, encryption, signing, transfer and
subsequent decryption can all be done outside of sett using available software
(GUI and command line). The only constraint is that the data package must
conform to the format required by sett as the metadata associated with the
encrypted package contains information used in routing and error checking.

More information about this format can be
found [here](/docs/sett/packaging_specifications/).

The process of encrypting and sending data using sett incorporates a number of
checks (Valid DTR ID, project and Data Manager match) which reduces the risk of
transfer failure. These checks are not available outside of the sett tool.

</details>

### 5\. Data Management

---

<details><summary><B>5.1. I need to use ABC program to analyse my data. How can
I request this program be available in the B-space?</B></summary>

Installation of standard programs and application stacks can be requested by a
BioMedIT project user by contacting their Data Manager and/or BioMedIT node
contact.\
\
BioMedIT project spaces support automated installation of various standard
bioinformatics application stacks _On-Demand_. \
\
Should the requested application/program not be in the list of supported
application stacks for automated _On-Demand_ installations, an application stack
can be made available to your project space after its careful evaluation by
designated BioMedIT node.

At the time of writing there is no possibility to include programs from
operating systems other than Linux.

</details>

<details><summary><B>5.2. Where are the data stored after the project is completed?</B></summary>

Long term storage and archiving of data is currently handled by the BioMedIT
node where the project is hosted. Please contact the BioMedIT node to discuss
this matter.

</details>

### 6. Security and Privacy

---

<details><summary><B>6.1. What is a SWITCH edu-ID and how do I get one?</B></summary>

A SWITCH edu-ID is an identity provider service offered by SWITCH, a Swiss
organization dedicated to providing internet services for the Swiss academic
community. It's designed to offer a single user identity for accessing various
online services within the academic and research spheres in Switzerland.

SWITCH edu-ID serves as a digital identity for students, researchers, and staff,
allowing them to access multiple online resources using a single set of
credentials. It simplifies the process of accessing various educational
services, such as libraries, research databases, learning management systems,
and more, by providing a unified login system.

It enhances security, streamlines access to educational resources, and
facilitates collaboration among individuals and institutions within the Swiss
academic community.

Links to creating a SWITCH edu-ID and other frequent operations can be found
[here](/docs/switch-edu-id/switch-edu-id/).

</details>

<details><summary><B>6.2. What does two factor authentication or two step login mean?</B></summary>

Two-step login enhances security by prompting users to specify a second
authentication factor when logging in. The second factor (your password is the
first factor), is either a code generated on your phone using an authenticator
app or sent to your mobile phone by SMS.

You may also know this feature as _two-factor authentication (2FA)_, _two-step
verification_ or _secure login_. Services that demand a higher level of
authentication security, e.g. those that enable account management or financial
transactions often require two-step login. Note: If neither of the above methods
work it is possible to use (a previously generated) one-time recovery code.

A guide to enabling 2FA can be found at [How can I enable/disable two-step
login?](https://help.switch.ch/eduid/faqs/?lang=en#mfa-enabling) on the SWITCH
website.

You should generate and safely store a set of recovery codes in case they are
ever needed.

</details>

<details><summary><B>6.3. I do not have an institutional email address (from a
university, hospital or similar). Can I still access the BioMedIT network?</B></summary>

You can access the landing page
at [https://biomedit.ch](https://biomedit.ch) and the BioMedIT Portal at
<https://portal.dcc.sib.swiss> (the portal only with a SWITCH edu-ID).

It is possible to set up a SWITCH edu-ID without an AAI - when creating the
account ensure that you select _Create without SWITCHaai_ -, but to gain access
to a B-space, you must provide be an institutional (non-personal) email address.
Members of a data provider organisation (not a university, university hospital
or similar) may be permitted to transfer data into the BioMedIT Network provided
that the organisation has been formally 'onboarded' to a BioMedIT node and the
user has a valid, signed public GPG key and valid SSH keys. There are work
instructions available to show you how to [create SSH keys under Windows or
Linux](/docs/sett/generating_ssh_keys/).

</details>

<details><summary><B>6.4. Why should I do the security awareness training?</B></summary>

In these courses, the users are introduced to data privacy regulations,
respective laws and practical concepts of information security, all important
subjects when dealing with confidential human data. After working through the
courses the user will have a better idea and an increased awareness of the need
for security when handling sensitive data.

</details>

<details><summary><B>6.5. Where can I find the courses?</B></summary>

You may find the links and instructions to enroll [here](/docs/training).

</details>

### 7. Working Groups

---

<details><summary><B>What special focus working groups are there on BioMedIT?</B></summary>

There are three main working groups on BioMedIT:

- **BioMedIT Interoperability Working Group**

Launched early 2018, the BioMedIT Interoperability Working Group aims to
streamline the computational process for researchers. The activities of the
working group are to develop and deploy solutions which abstract the underlying
IT infrastructure and enable researchers to focus on the development of novel
analyses.

- **BioMedIT IT Security Working Group**

A project oriented BioMedIT IT Security WG has been set up to address security
issues specifically relevant to the BioMedIT project. The SIB Data Protection
and Security Board is an overarching body responsible for establishing and
reviewing overall policy.

- **BioMedIT Data Management/Research support RDF Working Group**

The working group provides support to researchers in data management aspects
using the SPHN Semantic Framework and Semantic Web technologies. It is
responsible for building and implementing a support network for data management
with Semantic Web technology on BioMedIT that includes tools and services for
RDF, terminology service, documentation and user guide, and training on FAIR
health-related data for research.

For more information, visit the [BioMedIT
website](https://www.biomedit.ch/home/about-us/working-groups).

</details>

### 8\. SPHN

---

<details><summary><B>8.1. What is SPHN?</B></summary>

An initiative of the Swiss Government, the [Swiss Personalized Health
Network](https://sphn.ch/) is a national initiative under the leadership of the
Swiss Academy of Medical Sciences (SAMS). In collaboration with the SIB Swiss
Institute of Bioinformatics it contributes to the development, implementation
and validation of coordinated data infrastructures in order to make
health-relevant data interoperable and shareable for research in Switzerland. \
SPHN has adopted a federative approach by building upon – and supporting –
existing data sources and infrastructures across the country. To make health
data interoperable and accessible for research, SPHN rallies all decision-makers
from key clinical, research-, research support institutions and patient
organizations around the same table.

**Read the** [SPHN Fact-Sheet](https://sphn.ch/2023/10/12/sphn-factsheet-2023/).

</details>

<details><summary><B>8.2. How is SPHN related to BioMedIT?</B></summary>

Under the umbrella of the [SPHN](https://sphn.ch/) initiative a secure and
cutting-edge IT environment (BioMedIT network, a project of SIB) is being
established to support computational, biomedical research and clinical
bioinformatics, ensuring data privacy. In the same way as SPHN the BioMedIT
network builds on existing infrastructure and expertise in each of the
three BioMedIT nodes.

</details>
