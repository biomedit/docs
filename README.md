# BioMedIT Documentation

Project to generate the official
[BioMedIT documentation](https://biomedit.gitlab.io/docs).

## Development

The documentation is generated using [hugo](https://gohugo.io/)
(a static site generator) with [Docsy theme](https://www.docsy.dev/docs/).

### Without container

Prerequisites:

- [hugo](https://gohugo.io/installation/), an extended edition.
- [Go](https://go.dev/doc/install) needed for
  [hugo modules](https://gohugo.io/hugo-modules/).
  Note: it's not necessary to install Go as root - unpack the tar file anywhere
  in the filesystem and add the bin folder to your path.
- `node` latest [LTS release](https://github.com/nodejs/release#release-schedule)
  [nvm](https://github.com/nvm-sh/nvm) is a convenient tool for managing `node`
  installations.
- Install `node` dependencies `npm install` (within the project base directory).

Run `hugo` as server for local development (available at <http://localhost:1313>)

```bash
hugo server
```

Build production-ready static files

```bash
hugo
```

### Container

Using podman:

```bash
# Run `hugo` as server for local development (available at http://localhost:1313)
# This is the command to run to locally test the documentation.
podman run --rm -v $PWD:/src -p 1313:1313 docker.io/floryn90/hugo:ext-alpine server

# Build production-ready static files
podman run --rm -v $PWD:/src docker.io/floryn90/hugo:ext-alpine
```

Using docker:

```bash
# Run `hugo` as server for local development (available at http://localhost:1313)
docker run -u $(id -u) --rm -v $PWD:/src -p 1313:1313 docker.io/floryn90/hugo:ext-alpine server

# Build production-ready static files
docker run -u $(id -u) --rm -v $PWD:/src docker.io/floryn90/hugo:ext-alpine
```
