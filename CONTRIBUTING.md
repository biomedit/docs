# Contributing to sett-rs

## Development Setup

### Code style

This project uses [prettier](https://prettier.io/) for formatting,
[markdownlint](https://github.com/DavidAnson/markdownlint) for linting, and
[cspell](https://cspell.org/) for spell-checking of markdown files.

These tools can be directly integrated to your IDE. You can also run them on the
command line with `npm`.

```bash
npx prettier --check content/en/docs/
npx markdownlint-cli content/en/docs/
npx cspell content/en/docs/
```

## Commit Message Guidelines

This project follows the [Angular commit message
guidelines](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-guidelines)
for its commit messages.

### Template

Fields shown in `[square brackets]` are optional.

```text
type[(scope)]: subject line - max 100 characters

[body] - extended description of commit that can stretch over
multiple lines. Max 100 character per line.

[footer] - links to issues with (Closes #, Fixes #, Relates #)
```

### Type and keywords summary

#### Type

The following types are allowed. Only commits with types shown in **bold** are
automatically added to the changelog (and those containing the `BREAKING
CHANGE:` keyword):

- **feat**: add new content.
- **fix**: fix existing content
- build: changes that affect the build system or external dependencies.
- ci: changes to CI configuration files and scripts.
- docs: documentation only changes.
- refactor: code change that neither adds new content nor fixes existing
  content.
- style: change in code formatting only (no effect on functionality).

#### Scope

- name of the file/functionality/workflow affected by the commit.

#### Subject line

- one line description of commit with max 100 characters.
- use the imperative form, e.g. "add new feature" instead of "adds new feature"
  or "added a new feature".
- no "." at the end of subject line.

#### body

- Extended description of commit that can stretch over multiple lines.
- Max 100 characters per line.
- Explain things such as what problem the commit addresses, background info on
  why the change was made, alternative implementations that were tested but
  didn't work out.

#### footer

- Reference to git issue with `Closes/Close`, `Fixes/Fix`, `Related`.

### Examples

```text
feat(decrypt): add public key auto-download

Allows sett to auto-download public keys from the keyserver when encrypting/decrypting data
for/from recipients/signers whose key is not available in the user's local keyring.
This feature was added to make the life of users easier, so the encryption and decryption workflows
do not crash when a key is missing.

BREAKING CHANGE: config file was changed with the addition of a new "public_key_auto_download"
    option that enables auto-downloading of public keys from the default keyserver when set to
    'True'.

    To migrate a config file, follow the example below:

        Before:

        config: {
          gpg_dir: '~/.gnupg',
          certification_key: 'fingerprint',
        }

        After:

        config: {
          gpg_dir: '~/.gnupg',
          certification_key: 'fingerprint',
          allow_key_auto_download: 'False',
        }

Closes #7, #123, Related #23
```

```text
fix(gui): make warning message more explicit when key is not signed

Improves the readability of the message displayed to the user when a key is missing the
signature from the central authority.
The message was changed from:
    "Only keys signed by <key fingerprint> are allowed to be used."
to:
    "Only keys signed by <key user ID> <key fingerprint> are allowed to be used. Please get your
     public key signed by <key user ID>."

Closes #141
```

```text
docs(CONTRIBUTING): add examples of conventional commit messages

Add a few examples of conventional commit messages to CONTRIBUTING.md, so that people don't have
to click on the "Angular commit message guidelines" link to read the full specifications.
Add a concise summary of the guidelines to provider a reminder of the main types and keywords.

Closes #114, #139
```
